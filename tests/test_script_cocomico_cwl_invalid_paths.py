"""CWL entrypoint feature tests."""

from pathlib import Path

from pytest_bdd import given, scenario, then, when


@scenario("script_cocomico_cwl_invalid_paths.feature", "Invalid SBML directory")
def test_invalid_sbml_directory():
    """Invalid SBML directory."""


@scenario(
    "script_cocomico_cwl_invalid_paths.feature", "Invalid SBML directory is a file"
)
def test_invalid_sbml_directory_is_a_file():
    """Invalid SBML directory is a file."""


@scenario("script_cocomico_cwl_invalid_paths.feature", "Invalid model files")
def test_invalid_model_files():
    """Invalid model files."""


@scenario("script_cocomico_cwl_invalid_paths.feature", "Invalid seed file")
def test_invalid_seed_file():
    """Invalid seed file."""


@given("invalid SBML models directory", target_fixture="sbml_dir")
def _():
    """invalid SBML models directory."""
    return Path("tests") / "data_test" / "invalid_sbml"


@given("invalid SBML models directory is a file", target_fixture="sbml_dir")
def _():
    """invalid SBML models directory is a file."""
    return Path("tests") / "data_test" / "seeds.sbml"


@given("invalid model files", target_fixture="models")
def _():
    """invalid model files."""
    return [Path(f"invalid_{i}.sbml") for i in range(4)]


@given("invalid seed file", target_fixture="seed_file")
def _():
    """invalid seed file."""
    return Path("tests") / "data_test" / "invalid_seeds.sbml"


@when("run cocomico", target_fixture="result")
def _(seed_file, sbml_dir, models, helpers):
    """run cocomico."""
    # pylint: disable=R0913
    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        models=models,
    )
    return result


@then("fail wth error")
def _(result):
    """fail wth error."""
    assert result.exit_code != 0
    assert "is a file" in result.output or "does not exist" in result.output
