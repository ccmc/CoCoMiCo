"""CWL entrypoint feature tests."""

import json
from pathlib import Path

from pytest_bdd import given, scenario, then, when


@scenario("script_cocomico_cwl_dir.feature", "Analyze communities from a directory")
def test_analyze_communities_from_a_directory():
    """Analyze communities from a directory."""


@given("output path", target_fixture="output_json")
def _(tmp_path) -> Path:
    """output path."""
    return tmp_path / "results.json"


@when("run cocomico")
def _(seed_file, sbml_dir, output_json, helpers):
    """run cocomico."""

    result = helpers.invoke(
        "community", seed=seed_file, sbml=sbml_dir, json=output_json
    )
    assert result.exit_code == 0


@then("have result files")
def _(output_json):
    """have result files."""
    assert output_json.is_file()


@then("community from directory has expected taxa")
def _(sbml_dir, output_json):
    """community from directory has expected taxa."""
    expected_taxa = [f.stem for f in sbml_dir.iterdir()]

    with open(output_json, mode="r", encoding="UTF-8") as json_file:
        from_json = json.load(json_file)
        assert "taxa" in from_json
        for taxon in expected_taxa:
            assert taxon in from_json["taxa"]
