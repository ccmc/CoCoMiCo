"""Sampling of communities feature tests."""

import re
from collections import Counter
from itertools import chain
from pathlib import Path
from typing import Sequence

from pytest_bdd import given, scenario, then, when

from cocomico.community import Community
from sampling.generate import gen_added_value, gen_sample
from sampling.name import Name
from sampling.types import Community as SampleCommunity
from sampling.types import Ecosystem as SampleEcosystem
from sampling.types import NamedCommunity as SampleNamedCommunity

ECO_SZ = 10
NUM_ECO = 5


@scenario("sampling_generate.feature", "Sample from communities")
def test_sample_from_communities():
    """Sample from communities."""


@scenario("sampling_generate.feature", "Sample single communities")
def test_sample_single_communities():
    """Sample single communities."""


@scenario("sampling_generate.feature", "Trivial sampling")
def test_trivial_sampling():
    """Trivial sampling."""


@scenario("sampling_generate.feature", "Added value sample from communities")
def test_added_value_sample_from_communities():
    """Added value sample from communities."""


@scenario("sampling_generate.feature", "Sample pairs from community spec")
def test_sample_pairs_from_communities():
    """Sample pairs from communities."""


@scenario("sampling_generate.feature", "Sample using pool")
def test_using_pool():
    """Sample using pool."""


@scenario("sampling_generate.feature", "Added value sample from community definition")
def test_added_value_sample_from_community_definition():
    """Added value sample from community definition."""


@given("an ecosystem dict", target_fixture="ecosystem")
def _() -> Sequence[SampleNamedCommunity]:
    """an ecosystem dict."""
    ecos: Sequence[SampleNamedCommunity] = list(
        # Encode ecosystem in community name
        (
            Name(eco=f"eco{eco:02d}", size=ECO_SZ),
            [f"{(ECO_SZ * eco + i):02d}.sbml" for i in range(ECO_SZ)],
        )
        for eco in range(NUM_ECO)
    )
    assert len(list(ecos)) == NUM_ECO
    assert all(str(k)[0:3] == "eco" for k, _ in ecos)
    for k, coms in ecos:
        assert all(int(str(com)[0:2]) // ECO_SZ == int(str(k)[3:5]) for com in coms)
    return ecos


@given("a community list", target_fixture="community_list")
def _() -> list[SampleCommunity]:
    """a community list."""
    ecos: list[SampleCommunity] = list(
        [f"{(ECO_SZ * eco + i):02d}.sbml" for i in range(ECO_SZ)]
        for eco in range(NUM_ECO)
    )
    assert len(ecos) == NUM_ECO
    for eco, coms in enumerate(ecos):
        assert all(int(str(com)[0:2]) // ECO_SZ == eco for com in coms)
    return ecos


@given("a pool", target_fixture="pool")
def _() -> SampleCommunity:
    """a pool."""
    pool: SampleCommunity = [f"{9 * ECO_SZ + i:02d}.sbml" for i in range(ECO_SZ)]
    return pool


@when("sample communities equally", target_fixture="equal_samples")
def _(
    ecosystem: Sequence[SampleNamedCommunity], community_list: list[SampleCommunity]
) -> Sequence[SampleNamedCommunity]:
    """sample communities equally."""
    # Combine ecosystem dict and community list, check together.
    eco_sample = gen_sample(sources=ecosystem, sizes=[10], reps=10)
    com_sample = gen_sample(sources=community_list, sizes=[10], reps=10)
    return list(chain(eco_sample, com_sample))


@when("sample communities unequally", target_fixture="unequal_samples")
def _(ecosystem: Sequence[SampleNamedCommunity]) -> Sequence[SampleNamedCommunity]:
    """sample communities unequally."""
    # Only use ecosystem dict w.l.g.
    eco_sample = list(gen_sample(sources=ecosystem, sizes=[12], reps=10))
    return eco_sample


@when("sample communities unequally using pool", target_fixture="unequal_samples")
def _(
    ecosystem: Sequence[SampleNamedCommunity],
    pool: SampleCommunity,
) -> Sequence[SampleNamedCommunity]:
    """sample communities unequally."""
    # Only use ecosystem dict w.l.g.
    eco_sample = list(gen_sample(sources=ecosystem, sizes=[13], pool=pool, reps=10))
    return eco_sample


@when("sample single communities", target_fixture="single_samples")
def _(ecosystem: Sequence[SampleNamedCommunity]) -> Sequence[SampleNamedCommunity]:
    """sample single communities."""
    samples = list(
        chain.from_iterable(
            gen_sample(sources=[c], sizes=[5], reps=10) for _, c in ecosystem
        )
    )
    assert len(samples) == 50
    return samples


@when("sample ecosystems no sizes", target_fixture="trivial_samples")
def _(ecosystem: Sequence[SampleNamedCommunity]) -> Sequence[SampleNamedCommunity]:
    """sample ecosystems no sizes."""
    samples = list(
        chain.from_iterable(
            gen_sample(sources=[(k, c)], sizes=[], reps=1) for k, c in ecosystem
        )
    )
    assert len(samples) == 5
    return samples


@when("added value sample communities", target_fixture="added_samples")
def _(ecosystem: Sequence[SampleNamedCommunity]) -> Sequence[SampleNamedCommunity]:
    """added value sample communities."""
    ecosystem_coms = [c for _, c in ecosystem]
    pool = list(chain.from_iterable(ecosystem_coms[1:]))
    samples = list(gen_added_value(sources=[ecosystem[0]], pool=pool, reps=20))
    return samples


@when("create community objects", target_fixture="communities")
def _(community_json: Path, sbml_dir: Path) -> dict[str, Community]:
    """create community objects."""
    return Community.from_specification(spec=community_json, sbml_dir=sbml_dir)


@when("convert community definition to ecosystem", target_fixture="ecosystem")
def _(community_json: Path, sbml_dir: Path) -> Sequence[SampleNamedCommunity]:
    """convert community definition to ecosystem."""
    communities: dict[str, Community] = Community.from_specification(
        spec=community_json, sbml_dir=sbml_dir
    )
    ecos: Sequence[SampleNamedCommunity] = list(
        (Name.from_string(k), sorted(com.files.values()))
        for k, com in communities.items()
    )
    return ecos


@when("sample pairs from community spec", target_fixture="pairs")
def _(communities: dict[str, Community]) -> SampleEcosystem:
    """sample pairs from community spec."""
    com_list: list[SampleCommunity] = [
        sorted(c.files.values()) for c in communities.values()
    ]
    assert len(com_list) == 4

    pairs: SampleEcosystem = list(
        chain.from_iterable(
            gen_sample(sources=[com], sizes=[2], reps=10) for com in com_list
        )
    )
    assert len(list(pairs)) == 40
    return pairs


@then("samples have equally distributed origins")
def _(equal_samples: Sequence[SampleNamedCommunity]):
    """samples have equally distributed origins."""
    # equal_samples_l = list(equal_samples)
    # Total samples from 10 eco. dict and 10 comm. list should be 20.
    assert len(equal_samples) == 20
    # Samples are all of expected size
    assert all(len(c) == 10 for _, c in equal_samples)

    for _, sample in equal_samples:
        counts = Counter(int(str(model)[0:2]) // 10 for model in sample)
        # There should be 5 distinct ecosystems.
        assert set(counts.keys()) == set(range(5))
        # Size 10 samples of 5 ecosystems should have 2 per ecosystem.
        assert all(i == 2 for i in counts.values())


@then("unequal samples have an extra")
def _(unequal_samples: Sequence[SampleNamedCommunity]):
    """unequal samples have an extra."""
    # unequal_samples = list(unequal_samples)
    # Total samples from 10 eco. dict and 10 comm. list should be 20.
    assert len(unequal_samples) == 10
    # Samples are all of expected size
    assert all(len(c) == 12 for _, c in unequal_samples)

    for _, sample in unequal_samples:
        counts = Counter(int(str(model)[0:2]) // 10 for model in sample)
        # Size 12 samples of 5 ecosystems should have at least 2 per ecosystem.
        assert all(i >= 2 for i in counts.values())
        # At least one ecosystem is overrepresented
        assert sum(i > 2 for i in counts.values()) > 0


@then("sample names are as expected")
def _(equal_samples: Sequence[SampleNamedCommunity]):
    """sample names are as expected."""
    r_named = re.compile(r"(eco\d{2})_(\d+)_(\w+)_(\d+)_(\d+)")
    r_unnamed = re.compile(r"(X_[A-Z0-9]{32})_(\w+)_(\d+)_(\d+)")
    for k, _ in equal_samples:
        assert re.match(r_named, str(k)) or re.match(r_unnamed, str(k))


@then("samples have single origin")
def _(single_samples: Sequence[SampleNamedCommunity]):
    """samples have single origin."""
    single_samples = list(single_samples)
    # Samples are all of expected size
    assert all(len(c) == 5 for _, c in single_samples)

    for _, sample in single_samples:
        counts = Counter(int(str(model)[0:2]) // 10 for model in sample)
        assert len(counts.keys()) == 1


@then("samples equal ecosystems")
def _(
    trivial_samples: Sequence[SampleNamedCommunity],
    ecosystem: Sequence[SampleNamedCommunity],
):
    """samples equal ecosystems."""
    trivial_samples = list(trivial_samples)
    anon: Sequence[SampleNamedCommunity] = list(
        (Name(name=n, rep=None), c) for n, c in trivial_samples
    )

    for (n1, c1), (n2, c2) in zip(trivial_samples, anon):
        assert n1 == n2
        assert c1 == c2

    for (n1, c1), (n2, c2) in zip(trivial_samples, ecosystem):
        assert n1 == n2
        assert c1 == c2


@then("added value samples have added")
def _(
    added_samples: Sequence[SampleNamedCommunity],
    ecosystem: Sequence[SampleNamedCommunity],
):
    """added value samples have added."""
    counts = Counter(n.sub for n, _ in added_samples)
    assert counts["added"] == 20

    ecosystem_coms = [c for _, c in ecosystem]
    original = set(ecosystem_coms[0])
    pool = set(chain.from_iterable(ecosystem_coms[1:]))
    for sample in [set(c) for n, c in added_samples if n.sub == "added"]:
        extra = sample - original
        assert len(extra) == 1
        assert extra < pool
        assert not extra < original


@then("added value samples have all originals")
def _(
    added_samples: Sequence[SampleNamedCommunity],
    ecosystem: Sequence[SampleNamedCommunity],
):
    """added value samples have all originals."""
    counts = Counter(n.sub for n, _ in added_samples)
    assert counts["original"] == 1

    ecosystem_coms = [c for _, c in ecosystem]
    original = ecosystem_coms[0]
    assert original in [c for _, c in added_samples]


@then("added value samples have minus")
def _(
    added_samples: Sequence[SampleNamedCommunity],
    ecosystem: Sequence[SampleNamedCommunity],
):
    """added value samples have minus."""
    counts = Counter(n.sub for n, _ in added_samples)
    assert counts["minus"] > 0

    ecosystem_coms = [c for _, c in ecosystem]
    original = set(ecosystem_coms[0])
    for sample in [set(c) for n, c in added_samples if n.sub == "minus"]:
        assert len(original - sample) == 1


@then("pairs sample has two members")
def _(pairs: Sequence[SampleNamedCommunity]):
    """pairs sample has two members."""
    for _, com in pairs:
        assert len(com) == 2


@then("pairs sample members are from community")
def _(pairs: Sequence[SampleNamedCommunity], communities: dict[str, Community]):
    """pairs sample members are from community."""
    for _, (m1, m2, *_) in pairs:
        m1_found = set(k for k, c in communities.items() if m1 in c.files.values())
        m2_found = set(k for k, c in communities.items() if m2 in c.files.values())
        assert m1_found & m2_found


@then("the extras are from the pool")
def _(unequal_samples: Sequence[SampleNamedCommunity]):
    """the extras are from the pool."""
    for _, sample in unequal_samples:
        counts = Counter(int(str(model)[0:2]) // 10 for model in sample)
        # Every sample has 2 from the pool 9x.sbml
        assert counts[9] == 3


@then("added value samples have original names")
def _(
    added_samples: Sequence[SampleNamedCommunity],
    ecosystem: Sequence[SampleNamedCommunity],
):
    """added value samples have original names."""
    added = {(n.eco, n.ident): None for n, _ in added_samples if n.sub == "original"}
    original = {(n.eco, n.ident): None for n, _ in ecosystem[:1]}

    assert added == original
