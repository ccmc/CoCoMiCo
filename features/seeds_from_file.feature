Feature: Seeds class
  Represent a collection of seeds

  Scenario Outline: Read seeds from file
    Given a seed definition <file>
    When create seeds object
    Then object contains expected <metabolite_biomolecules>
    And object has the name <name>

    Examples:
    | file                    | metabolite_biomolecules  | name       |
    | seeds.sbml              | M_E_c,M_F_c,M_X1_c       | seeds      |
    | seeds_d/seeds_id.sbml   | M_E_c,M_F_c,M_X1_c       | some_seeds |
    | seeds_d/seeds_name.sbml | M_E_c,M_F_c,M_X1_c       | some_name  |
    | seeds_d/seeds_both.sbml | M_E_c,M_F_c,M_X1_c       | some_name  |
    | seeds_d/seeds_none.sbml | M_E_c,M_F_c,M_X1_c       | none       |

