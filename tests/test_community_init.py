"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import parsers, scenario, then, when

from cocomico.community import Community


@scenario("community_init.feature", "Initialize a community")
def test_initialize_a_community():
    """Initialize a community."""


@when(
    parsers.parse("create community object from {models:S}"), target_fixture="community"
)
def _(sbml_dir: Path, models: str) -> Community:
    """create community object from <models>."""
    if models == "None":
        return Community()

    ids = models.split(",")
    files = [Path(f"{id}.sbml") for id in ids]
    community = Community(models=files, sbml_dir=sbml_dir)
    assert set(community.keys()) == set(ids)
    return community


@then(
    parsers.parse(
        "the community has expected {taxa:S} {products:S} {reactants:S} {reactions:S}"
    )
)
def _(community: Community, taxa, products, reactants, reactions):
    """each <community> has expected <models> <taxa> <products> <reactants>."""
    if taxa == "None":
        return
    assert community.taxa == set(taxa.split(","))
    assert {m.biomolecule for m in community.products} == set(products.split(","))
    assert {m.biomolecule for m in community.reactants} == set(reactants.split(","))
    assert community.biomolecule >= {*community.products, *community.reactants}
    assert {r.name for r in community.reactions} == set(reactions.split(","))
    #
    assert {r.taxon for r in community.reactions} <= community.taxa
    assert {m.provenance for m in community.products} <= community.taxa
    assert {m.provenance for m in community.reactants} <= community.taxa
    #
    assert community.products.biomolecules == set(products.split(","))
    assert community.reactants.biomolecules == set(reactants.split(","))
    assert community.products.taxa <= community.taxa
    assert community.reactants.taxa <= community.taxa
