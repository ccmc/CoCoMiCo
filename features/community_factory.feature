Feature: Community class
  Represent a microbial community

  Scenario Outline: Initialize a set of communities
    Given a community definition file
    And SBML models directory
    When create community objects
    Then each <community> has expected <taxa>
    And community files match the definition

    Examples:
    | community | taxa                                |
    | com_0     | Com1Org1,Com1Org2,Com1Org3          |
    | com_1     | Com2Org1,Com2Org2,Com2Org3,Com2Org4 |
    | com_2     | Com1Org1,Com2Org4                   |
    | com_3     | Com1Org1,Com2Org4                   |
