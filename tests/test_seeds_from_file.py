"""Seeds class feature tests."""

from pathlib import Path

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Seeds


@scenario("seeds_from_file.feature", "Read seeds from file")
def test_initialize_seeds():
    """Read seeds from file."""


@given(parsers.parse("a seed definition {file:S}"), target_fixture="seed_path")
def _(file: str) -> Path:
    """a seed definition <file>."""
    return Path("tests") / "data_test" / file


@when("create seeds object", target_fixture="seeds")
def _(seed_path: Path) -> Seeds:
    """create seeds object."""
    return Seeds.from_file(seed_path)


@then(parsers.parse("object contains expected {metabolite_biomolecules:S}"))
def _(seeds: Seeds, metabolite_biomolecules: str):
    """object contains expected <metabolite_biomolecules>."""
    assert set(metabolite_biomolecules.split(",")) == seeds


@then(parsers.parse("object has the name {name:S}"))
def _(seeds: Seeds, name: str):
    """object has the name <name>."""
    assert seeds.name == name
