# Authors

  - Maxime Lecomte - https://team.inria.fr/pleiade
  - Coralie Muller - https://team.inria.fr/pleiade
  - Ariane Badoual - https://team.inria.fr/pleiade
  - Hélène Falentin - https://stlo.rennes.hub.inrae.fr
  - David James Sherman - https://team.inria.fr/pleiade
  - Clémence Frioux - https://team.inria.fr/pleiade
