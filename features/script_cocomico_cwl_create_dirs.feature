Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Create output directory if needed for example
    Given a missing output directory path
    And a working directory    
    When change to working directory
    And run cocomico example 
    Then output directory is created
    And have result files
   
  Scenario Outline: Create output directory if needed for one community
    Given a missing output directory path
    And a working directory    
    And seed file
    And SBML models directory
    And model files
    And csv output path
    When change to working directory
    And run cocomico for one community
    Then output directory is created
    And have result files
