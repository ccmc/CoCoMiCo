"""
Constants common to CoCoMiCo.
"""

CSV_HEADER = [
    "community",
    "seeds",
    "delta",
    "coop",
    "rho",
    "comp",
    "comp_num",
    "coop_num",
    "coop_prod",
    "coop_cons",
    "size",
]

COOP = "cooperation"
COOP_NUM = "number of exchanged metabolites"
COOP_PROD = "coop bonus producers"
COOP_CONS = "coop bonus consumers"

COMP = "competition"
COMP_NUM = "number of polyopsonist metabolites"

RHO = "added activated community"
RHO_CACTIV = "sum community activated"
RHO_IACTIV = "sum individual activated"

DELTA = "added value community"
DELTA_CSCOPE = "sum community scope"
DELTA_ISCOPE = "sum individual scope"

COM_SIZE = "community size"
