Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Analyze communities from specification
    Given seed file
    And SBML models directory
    And a community specification file
    And output directory path
    When run cocomico
    Then have result files
    And each <community> has expected <taxa>

    Examples:
    | community | taxa                                |
    | com_0     | Com1Org1,Com1Org2,Com1Org3          |
    | com_1     | Com2Org1,Com2Org2,Com2Org3,Com2Org4 |
    | com_2     | Com1Org1,Com2Org4                   |
    | com_3     | Com1Org1,Com2Org4                   |
