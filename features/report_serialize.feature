Feature: Report
  Extract reports from community results

  Scenario Outline: Serialization
  Given community of <organisms>
  And SBML models directory
  And <seed_biomolecules>
  When build community
  And compute results for seeds	
  And serialize community
  And serialize results
  Then community and serialization agree
  And community and serialized analyses agree
  And community and serialized subsets agree
  And community and serialized scores agree

  Examples:
  | organisms                                    | seed_biomolecules  |
  | Com1Org1,Com1Org2,Com1Org3                   | M_E_c,M_F_c,M_X1_c | 
  | Com1Org1,Com2Org4                            | M_E_c,M_F_c,M_X1_c | 
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4          | M_E_c,M_F_c,M_X1_c | 
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | M_E_c,M_F_c        | 
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | M_E_c,M_F_c,M_X1_c | 
  | Com3Org1,Com3Org2,Com3Org3,Com3Org4          | M_E_c,M_F_c,M_X1_c | 
  | Com3Org1,Com3Org2,Com3Org4                   | M_E_c,M_F_c,M_X1_c | 
