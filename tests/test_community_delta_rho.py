"""Community feature tests."""

from pytest import approx
from pytest_bdd import parsers, scenario, then

from cocomico import score
from cocomico.base import Seeds
from cocomico.community import Community


@scenario("community_delta_rho.feature", "Compute metrics delta rho")
def test_compute_metrics_delta_rho():
    """Compute metrics delta rho."""


@then(
    parsers.parse(
        "each community has the expected "
        "{delta:g} {cscope:g} {iscope:g} {rho:g} {cactiv:g} {iactiv:g}"
    )
)
def _(
    community: Community,
    seeds: Seeds,
    delta: int,
    cscope: int,
    iscope: int,
    rho: int,
    cactiv: int,
    iactiv: int,
):
    """each community has the expected <delta> <cscope> <iscope> <rho> <cactiv> <iactiv>."""
    # pylint: disable=R0913

    # Delta is the added value in terms of scope size
    computed_delta, computed_delta_metrics = score.delta(community, seeds)
    assert approx(delta, 0.01) == computed_delta
    assert computed_delta_metrics["added value community"] == delta
    assert computed_delta_metrics["sum community scope"] == cscope
    assert computed_delta_metrics["sum individual scope"] == iscope

    # Rho is the added value in terms of activated reaction × taxon tuples
    computed_rho, computed_rho_metrics = score.rho(community, seeds)
    assert approx(rho, 0.01) == computed_rho
    assert computed_rho_metrics["added activated community"] == rho
    assert computed_rho_metrics["sum community activated"] == cactiv
    assert computed_rho_metrics["sum individual activated"] == iactiv
