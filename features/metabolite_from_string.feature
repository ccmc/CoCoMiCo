Feature: Metabolite class
    Represent a metabolite tagged with a taxon

    Scenario Outline: Initialize a metabolite from string
        Given a <metabolite_id> string
        When create metabolite
        Then metabolite has expected <provenance> and <biomolecule>

        Examples:
        | metabolite_id   | provenance | biomolecule       |
        | M_A_c.1         | 1      | M_A_c      |
        | M_A_c.Org2      | Org2   | M_A_c      |
        | M_B_c.Org2      | Org2   | M_B_c      |
        | M_OleCoA_c.SACE | SACE   | M_OleCoA_c |
        | M_C_c.          | _none_ | M_C_c      |
        | M_C_c           | _none_ | M_C_c      |
