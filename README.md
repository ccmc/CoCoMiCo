# CoCoMiCo : COoperation and COmpetition in MIcrobial COmmunities

CoCoMiCo aims at characterising the metabolism of microbial
communities through genome-scale metabolic networks: it calculates
cooperation and competition potentials for the communities.

It has four modes:
* a _command-line_ mode [`community`](#cocomico.cwl.community),
  where analyses are performed on communities defined using command-line arguments,
* a _batch_ mode [`specification`](#cocomico.cwl.specification),
  where analyses are performed according to JSON specification files,
  formatted as described below,
* a [_CWL_](https://www.commonwl.org/) mode [`entrypoint`](#cocomico.cwl.entrypoint),
  designed to be called by CWL workflows using the definitions in
  [`tools/`](tools/), and
* an _example_ mode [`example`](#cocomico.cwl.example),
  that runs CoCoMiCo on a bundled example community.

## Install

Required **Python >= 3.10**. The main dependency of CoCoMiCo is an
Answer Set Programming (ASP) solver.
[Clyngor](https://github.com/Aluriak/clyngor) manages the connection
between Python and ASP; the
[Clyngor_with_clingo](https://github.com/Aluriak/clyngor-with-clingo/)
package includes the solver binaries in the Python environment so that
they do not need to be installed separately.
If you work in a Conda environment, installing the solvers directly
([clingo](https://anaconda.org/potassco/clingo)) makes it unnecessary
to install Clyngor_with_clingo.

Install from Pypi using

```sh
pip install cocomico
```

or after cloning this repository

```sh
pip install .
```

**If you use CoCoMiCo, please cite:**

Lecomte M, Muller C, Badoual A, Falentin H, Sherman DJ, and Frioux C. 2023. CoCoMiCo: metabolic modelling of cooperation and competition potentials in large-scale bacterial communities.

## Usage

### Single community run <a name="cocomico.cwl.community"></a>

`community` defines a single community on the command line, and
expects SBML-formatted metabolic network files in a single directory:

```
Community_folder
├── species_1.sbml
├── species_4.sbml
├── species_10.sbml
```


```
Usage: cocomico community [OPTIONS] [MODEL]...

  CoCoMiCo for a community defined by the MODELs, or by all models in
  sbml_dir.

Options:
  --output PATH                   Directory to write output, created if
                                  necessary
  --seeds-file PATH               SBML file defining seed biomolecules
  --seeds-def TEXT                Comma-separated list of seed biomolecules
  --sbml-dir DIRECTORY            Directory containing SBML model files
  --csv-output PATH               Path to write CSV output
  --header-csv / --no-header-csv  Add header to CSV output
  --name TEXT                     Community name (and default output filename
                                  stem)
  --json-output PATH              Path to write a single JSON output or '-'
  --help                          Show this message and exit.
```

#### Example execution

```
cocomico community
    --seeds-def M_E_c,M_F_c,M_X1_c
    --seeds-file seeds.sbml
    --json-output results.json
    --csv-output results.csv
    sbml/Com2Org*.sbml
```

### Multiple communities using a specification <a name="cocomico.cwl.specification"></a>

`specification` uses a JSON-formatted specification file to define
collections of communities defined over a set of SBML model files:

```
Folder_input
├── communities.json
├── sbml
│   └── species_1.sbml
│   └── species_4.xml
|    ..
```

`communities.json`` must be in the following format:

```
{
    "com_0" :[
            species_1.sbml,
            species_4.sbml,
            species_10.sbml
    ],
    "com_1" :[
            species_12.sbml,
            species_120.sbml
    ]
}

```

Here, sample community `com0` is composed of three species:
`species_`, `species_4` and `species_10`.

```
Usage: cocomico specification [OPTIONS]

  CoCoMiCo for all communites defined by a specification file.

Options:
  --output PATH                   Directory to write output, created if
                                  necessary
  --seeds-file PATH               SBML file defining seed biomolecules
  --seeds-def TEXT                Comma-separated list of seed biomolecules
  --sbml-dir DIRECTORY            Directory containing SBML model files
  --csv-output PATH               Path to write CSV output
  --header-csv / --no-header-csv  Add header to CSV output
  --spec PATH                     JSON file that specifies communities
  --help                          Show this message and exit
```

#### Example execution

```
cocomico specification
    --spec communities.json
    --seeds-file seeds.sbml
    --output results
```

### Bundled example <a name="cocomico.cwl.example"></a>

CoCoMiCo bundles a simple example that can be run without supplying
input SBML models.

```
Usage: cocomico example [OPTIONS]

  CoCoMiCo for a small example community.

  cocomico example --output results

Options:
  --output PATH  Directory to write output, created if necessary
  --help         Show this message and exit.
```

## Run using CWL <a name="cocomico.cwl.entrypoint"></a>

CWL
[CommandLineTool](https://www.commonwl.org/user_guide/topics/command-line-tool.html)
definitions are provided for CoCoMiCo in
[`tools/cocomico/`](tools/cocomico/).

To run CoCoMiCo in single community mode, use
[`cocomico.cwl`](tools/cocomico/cocomico.cwl) and supply a job file
like [`cocomico_job.yaml`](tools/cocomico/cocomico_job.yaml)
that specifies which model files to include in the community:

```sh
cwltool --verbose --no-container \
    tools/cocomico/cocomico.cwl tools/cocomico/cocomico_job.yaml
```

The job file specifies which output to compute, in the example
`results.json` and `results.csv`.

To run CoCoMiCo in specification mode, use
[`cocomico_spec.cwl`](tools/cocomico/cocomico_spec.cwl) and supply a
job file like
[`cocomico_spec_job.yaml`](tools/cocomico/cocomico_spec_job.yaml) that
specifies a specification file:

```sh
cwltool --verbose --no-container \
    tools/cocomico/cocomico_spec.cwl tools/cocomico/cocomico_spec_job.yaml
```

The CWL definitions assume that software requirements are already met,
so the Python virtual environment must be activated, and CoCoMiCo
installed.
If a CWL runner is not already installed on your system, you can add
the reference implementation using `pip install cwltool` or
`conda install cwltool`.

### Record provenance in a RO-Crate

The [**cwltool**](https://pypi.org/project/cwltool/) reference
implementation can record result provenance in a
[Research Object Crate](https://www.researchobject.org/ro-crate/):

```sh
cwltool --provenance ro-crate --verbose --no-container \
    tools/cocomico/cocomico_spec.cwl tools/cocomico/cocomico_spec_job.yaml
```

The RO-Crate will contain the results, a packed CWL workflow, metadata
describing the CoCoMiCo execution, and a copy of the input data.

The recorded results always can be reconstructed by rerunning the workflow:

```sh
cwltool --no-container workflow/packed.cwl workflow/primary-job.json
```

## Output

Summary results will be reported in a single tab-separated CSV file,
for example

```
community  seeds               delta  coop  rho   comp  comp_num  coop_num  coop_prod  coop_cons
com_0      M_E_c,M_F_c,M_X1_c  12     9.0   21    0.6   1         4         4.5        4.5
com_1      M_E_c,M_F_c,M_X1_c  19     17.0  35    1.0   2         8         8.5        8.5
com_2      M_E_c,M_F_c,M_X1_c  5      2.0   10    1.0   1         1         1.0        1.0
```

The analysis for each community will be a JSON file containing, for
example:

```json
{
    "models": [
        "Com1Org1.sbml",
        "Com1Org2.sbml",
        "Com1Org3.sbml"
    ],
    "results": [
        {
            "activated": [  ],
            "competition": [
                0.6666666666666666,
                {
                    "competition": 0.6666666666666666,
                    "number of polyopsonist metabolites": 1
                }
            ],
            "consumed_seeds": [
                "M_E_c",
                "M_F_c",
                "M_X1_c"
            ],
            "cooperation": [
                9.0,
                {
                    "coop bonus consumers": 4.5,
                    "coop bonus producers": 4.5,
                    "cooperation": 9.0,
                    "number of exchanged metabolites": 4
                }
            ],
            "delta": [
                12,
                {
                    "added value community": 12,
                    "sum community scope": 23,
                    "sum individual scope": 11
                }
            ],
            "exchange": {
                "M_A_c": {
                    "consumers": [
                        "Com1Org3"
                    ],
                    "producers": [
                        "Com1Org2"
                    ]
                },
                "M_B_c": {
                    "consumers": [
                        "Com1Org1"
                    ],
                    "producers": [
                        "Com1Org3"
                    ]
                },
                "M_C_c": {
                    "consumers": [
                        "Com1Org2",
                        "Com1Org3"
                    ],
                    "producers": [
                        "Com1Org1",
                        "Com1Org3"
                    ]
                },
                "M_Y_c": {
                    "consumers": [
                        "Com1Org3"
                    ],
                    "producers": [
                        "Com1Org2"
                    ]
                }
            },
            "monopsonist": {
                "M_A_c": 1,
                "M_B_c": 1,
                "M_Y_c": 1
            },
            "polyopsonist": {
                "M_C_c": 2
            },
            "produced_seeds": [
                "M_F_c"
            ],
            "rho": [
                21,
                {
                    "added activated community": 21,
                    "sum community activated": 38,
                    "sum individual activated": 17
                }
            ],
            "scope": [  ],
            "seeds": [
                "M_E_c",
                "M_F_c",
                "M_X1_c"
            ]
        }
    ],
    "sbml_dir": "example/sbml",
    "taxa": [
        "Com1Org1",
        "Com1Org2",
        "Com1Org3"
    ]
}
```


## Logging

Each step of the analysis is logged to the console.
The default log level is INFO.
To show a detailed trace, specify a log level of DEBUG.
To only show warnings or worse, specify a log level of WARN.

```sh
cocomico --loglevel=WARN community --seeds-def M_E_c,M_F_c,M_X1_c sbml/Com2Org*.sbml
```


## Random sampling

CoCoMiCo includes `sample-communities`, a utility for generating community specification files using stratified random sampling from multiple sources. Sources can be directories of SBML models, or community specifications. If more than one source is given, equal numbers of models will be chosen from each source: the stratified samples are uniform., rounded out by choosing from a pool. If a pool is not provided explicitly, then it is made by combining all sources.

You must specify the _sizes_ of the communities, and the number of _reps_ of each size.
Unique sample names are generated by hashing their members, but can also be generated sequentially from a _name_ prefix.

To help with cross validation, samples can be completed with a leave-one-out and an add-one-in strategy. For each sample of size _N_, _N_ leave-one-out samples and _reps_ add-one-in samples will be generated in addition to the original samples.

The [`sample`](#sampling.cwl.sample) command generates one sample of communities.

```
sample-communities sample --dest mix --size 10 leaf root soil
sample-communities sample --dest mix --size 10 --source leaf --source root --source soil

sample-communities sample --dest eco --size 10 ecosystem_*.json
sample-communities sample --dest av-gut --added-value --pool sbml --source sample/gut
sample-communities sample --dest av-max --added-value --source mix.json
```

The [`sample-chain`](#sampling.cwl.sample_chain) command builds samples iteratively.

```
sample-communities \
  sample-chain --dest mix --size 10 --source leaf --source root \
  sample-chain --dest eco --size 10 --added-value --source mix

sample-communities sample-chain --dest av --added-value --pool sbml --source sample/gut
```

Run `sample-communities --help` for detailed options.

CWL CommandLineTool definitions for sampling are provided [`tools/cocomico/`](tools/cocomico/).

# Version
CoCoMiCo version 0.3.2
