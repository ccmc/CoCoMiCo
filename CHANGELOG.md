# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.3] - 2024-07-03

### Added

- Object representations and new API
	- A _Community_ is a collection of (SBML) models, with additional operations to calculate metabolic scope and characterize exchanges
	- Base objects _Taxon_, _Biomolecule_, _Reaction_, _Seeds_
	- A _Metabolite_ is a **biomolecule with provenance**
	- An _Exchange_ compactly represents a relation between $N$ producers and $M$ consumers
	- A _Model_ stores bipartite metabolite-reaction relations
    - An _Answer_ records results from ASP and can be cached by a _Community_
- [Common Workflow Language](https://commonwl.org) (CWL) tool definition
- Complete static type hinting, checked by [Mypy](https://mypy.readthedocs.io)
- Significant number of Gherkin-defined tests
- Static code quality assessment using [Sonarqube](https://www.sonarsource.com/products/sonarqube/)

### Changed

- Generalized scope [`general_scope.lp`](src/cocomico/encodings/general_scope.lp) works for any subset of taxa, including individuals
- CLI now uses Click and provides four subcommands
	- **entrypoint** is designed for CWL
	- **community** analyze one community defined on the command line
	- **specification** analyze all communities defined by a specification file
	- **example** analyze a small example community
- Packaging uses [Hatch](https://hatch.pypa.io/)

### Deprecated

- [`scope.lp`](src/cocomico/encodings/scope.lp) old-style scope with separate definitions for `pscope`, `escope`, `iscope`

### Removed

- CLI from v0.2

### Fixed

- Computation of _limiting_ used wrong cardinality

## [v0.2] - 2023-01-19

Initial release
