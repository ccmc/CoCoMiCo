Feature: Metabolite class
    Represent a metabolite tagged with a taxon

    Scenario Outline: Initialize a metabolite from provenance and biomolecule
        Given a <provenance> taxon
        And a metabolite <biomolecule>
        When create metabolite
        Then metabolite has the same provenance
        And metabolite has the same biomolecule
        And metabolite has expected <string>

        Examples:
        | provenance | biomolecule       | string          |
        | 1      | M_A_c      | M_A_c.1         |
        | Org2   | M_A_c      | M_A_c.Org2      |
        | Org2   | M_B_c      | M_B_c.Org2      |
        | SACE   | M_OleCoA_c | M_OleCoA_c.SACE |
