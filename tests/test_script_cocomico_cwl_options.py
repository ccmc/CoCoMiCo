"""CWL entrypoint feature tests."""

from pathlib import Path

from pytest_bdd import given, scenario, then, when


@scenario("script_cocomico_cwl_options.feature", "Check output options")
def test_check_output_options():
    """Check output options."""


@given("with_csv output path", target_fixture="output_with_csv")
def _(tmp_path):
    """with_csv output path."""
    (path := tmp_path / "with_csv").mkdir(exist_ok=True)
    return path


@given("with_json output path", target_fixture="output_with_json")
def _(tmp_path):
    """with_json output path."""
    (path := tmp_path / "with_json").mkdir(exist_ok=True)
    return path


@given("without_csv output path", target_fixture="output_without_csv")
def _(tmp_path):
    """without_csv output path."""
    (path := tmp_path / "without_csv").mkdir(exist_ok=True)
    return path


@given("without_json output path", target_fixture="output_without_json")
def _(tmp_path):
    """without_json output path."""
    (path := tmp_path / "without_json").mkdir(exist_ok=True)
    return path


RESULT_JSON = "result.json"
RESULT_CSV = "result.csv"


@when("run cocomico with csv")
def _(seed_file, sbml_dir, models, output_with_csv, helpers):
    """run cocomico with csv."""

    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        json=output_with_csv / RESULT_JSON,
        csv=output_with_csv / RESULT_CSV,
        models=models,
    )
    assert result.exit_code == 0


@when("run cocomico with json")
def _(seed_file, sbml_dir, models, output_with_json, helpers):
    """run cocomico with json."""

    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        json=output_with_json / RESULT_JSON,
        csv=output_with_json / RESULT_CSV,
        models=models,
    )
    assert result.exit_code == 0


@when("run cocomico without csv")
def _(seed_file, sbml_dir, models, output_without_csv, helpers):
    """run cocomico without csv."""

    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        json=output_without_csv / RESULT_JSON,
        models=models,
    )
    assert result.exit_code == 0
    assert not (Path(".") / "erehwon.csv").is_file()


@when("run cocomico without json")
def _(seed_file, sbml_dir, models, output_without_json, helpers):
    """run cocomico without json."""

    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        csv=output_without_json / RESULT_CSV,
        models=models,
    )
    assert result.exit_code == 0
    assert not (Path(".") / "erehwon.json").is_file()


@then("have expected result files")
def _(
    output_without_json,
    output_with_json,
    output_without_csv,
    output_with_csv,
):
    """have expected result files."""
    assert not (output_without_json / RESULT_JSON).is_file()
    assert (output_with_json / RESULT_JSON).is_file()
    assert not (output_without_csv / RESULT_CSV).is_file()
    assert (output_with_csv / RESULT_CSV).is_file()
