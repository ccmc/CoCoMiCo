#!/usr/bin/env cwl-runner
class: CommandLineTool
cwlVersion: v1.2

id: "sample_communities"
label: "sample_communities"

hints:
  - class: DockerRequirement
    dockerPull: quay.io/biocontainers/python:3.10.4
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.sources)  # allow relative paths

inputs:
  sources:
    type:
      type: array
      items:
        - File
        - Directory
      inputBinding:  # convert to relative paths
        prefix: "--source"
        separate: true
        valueFrom: $(self.basename)
    inputBinding:
      position: 5
    doc: |
      Source ecosystems: directories of models or
      JSON community specifications
  dest:
    type: string
    doc: |
      Name of destination directory
    inputBinding:
      position: 3
      prefix: "--dest=_out/"
      separate: False
    default: "eco"
  name:
    type: string?
    doc: |
      Output ecosystem name
    inputBinding:
      position: 3
      prefix: "--name"
  reps:
    type: int?
    doc: |
      Number of samples
    inputBinding:
      position: 3
      prefix: "--reps"
  added_value:
    type: boolean
    doc: |
      Also make minus/added communities
    default: False
    inputBinding:
      position: 3
      valueFrom: ${if (self) { return "--added-value"; } else { return "--no-added-value"; }}
  size:
    doc: |
      Sample sizes
    default: []
    type:
      type: array
      items: int
      inputBinding:
        prefix: "--size"
        separate: true
    inputBinding:
      position: 3

  pool:
    type: Directory?
    doc: |
      Pool to sample from
    inputBinding:
      position: 3
      prefix: "--pool"
  random-seed:
    type: int?
    doc: |
      Random generator seed
    inputBinding:
      position: 1
      prefix: "--random-seed"
  models-per-spec:
    type: int?
    doc: |
      Split output specs ≤ N models
    inputBinding:
      position: 1
      prefix: "--models-per-spec"
  random-state:
    type: File?
    doc: |
      Pickled random generator state
    inputBinding:
      position: 1
      prefix: "--random-state"
  loglevel:
    type:
      type: enum
      symbols: ["DEBUG", "INFO", "WARN"]
    doc: |
      Logging level INFO, DEBUG, WARN
    default: INFO
    inputBinding:
      position: 1
      prefix: "--loglevel"

outputs:
  sample:
    type: Directory
    doc: |
      Complete dest directory with all generated JSON outputs
    outputBinding:
      glob: "$(runtime.outdir)/_out/$(inputs.dest)"
  sample_json:
    type: File
    doc: |
      Single JSON output, first in lexicographic order if several
    outputBinding:
      glob: "$(runtime.outdir)/_out/$(inputs.dest)/*.json"
      outputEval: "$(self.sort()[0])"
  initial-random-state:
    type: File
    doc: |
      Inital state of random number generator, for reproducibile reexecution
    outputBinding:
      glob: "$(runtime.outdir)/_out/$(inputs.dest)/random-initial.pickle"
  final-random-state:
    type: File
    doc: |
      Final state of random number generator, for reproducible chaining
    outputBinding:
      glob: "$(runtime.outdir)/_out/$(inputs.dest)/random-final.pickle"
  standard_output:
    type: stdout
  standard_error:
    type: stderr

baseCommand:
  - "sample-communities"

arguments:
  - valueFrom: "sample"
    position: 2

stdout: stdout.txt
stderr: stderr.txt

doc: |
  Usage: sample_communities.cwl sample_job.yaml

    Generate communities by stratified random sampling from multiple sources and
    a general pool, possibly with minus one / added one post-processing.

  Inputs:
    sources           Source ecosystems in directories or community files
    name              Output ecosystem name
    reps              Number of samples
    added_value       Also make minus/added communities
    size              Sample sizes
    pool              Pool ecosystem to sample from
    dest              Destination name
    models-per-spec   Split output specs <= N models
    random-seed       Random generator seed
    random-state      Pickled random generator state
    loglevel          Logging level INFO, DEBUG, WARN

s:author:
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005

s:codeRepository: https://gitlab.inria.fr/CCMC/CoCoMiCo.git
s:license: https://spdx.org/licenses/GPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2023-08-29"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.25.owl
