Feature: SBML reader
  Read and parse SBML model files

  Scenario Outline: Read models
    Given SBML models directory
    And an sbml reader
    When read model from <file>
    Then the file maps to expected <taxon>
    And the model has the expected <reactions>

    Examples:
    | file | taxon | reactions |
    | Com1Org1.sbml | Com1Org1 | R_B_JD,R_D_X,R_X_G,R_G_H,R_I_H,R_K_H,R_H_C,R_F2_C,R_X1_F2,R_D_Xrev,R_X_Grev,R_G_Hrev,R_I_Hrev,R_F2_Crev |
    | Com1Org2.sbml | Com1Org2 | R_F_M,R_N_M,R_M_O,R_O_P,R_M_Q,R_Q_A,R_A_S,R_T_S,R_C_PY,R_F_Mrev,R_O_Prev,R_M_Qrev,R_Q_Arev,R_A_Srev |
    | Com1Org3.sbml | Com1Org3 | R_E_V,R_V_B,R_W_B,R_AZ_W,R_Y_W,R_W_A2,R_A2_B2,R_C2_B2,R_C_C2,R_V_Brev,R_W_A2rev,R_A2_B2rev,R_C2_B2rev,R_C_C2rev |

  Scenario Outline: Read models several times
    Given SBML models directory
    And model files
    And an sbml reader
    When read the files several times
    Then the reader records each file only once
    And the reader records each taxon only once

  Scenario Outline: Read model without sbml_dir
    Given SBML models directory
    And an sbml reader without sbml_dir
    When read a set of models
    Then the set of taxa are correct

  Scenario Outline: Read compressed model
    Given SBML models directory
    And an sbml reader without sbml_dir
    When read compressed models
    Then the set of taxa are correct
