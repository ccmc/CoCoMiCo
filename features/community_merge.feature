Feature: Community class
  Represent a microbial community

  Scenario: Merge models into a community
    Given SBML models directory
    And a multi-model community
    When merge models into a new community
    Then the merged community has the same models
    And the merged community has the same properties
