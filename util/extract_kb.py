# -*- coding: utf-8 -*-

"""
Extract facts from knowledge bases into files, for easier testing.
"""

# pylint: skip-file

from pathlib import Path

from cocomico.community import Community

sbml_dir = Path("..") / "sbml"
kb_dir = Path(".")

for file in sbml_dir.iterdir():
    community = Community(models=[file], sbml_dir=sbml_dir)
    lp_path = kb_dir / f"{ ''.join(community.keys()) }.lp"
    with open(lp_path, mode="w+", encoding="UTF-8") as lp:
        for fact in sorted([str(a) for a in community.knowledge_base]):
            lp.write(f"{ fact }.\n")
