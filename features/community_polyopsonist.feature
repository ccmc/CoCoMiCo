Feature: Community class
    Represent a microbial community

    Scenario Outline: Identify polyopsonist metabolites
        Given community of <organisms>
        And SBML models directory
        And <seeds>
        When build community
        And compute polyopsonist wrt seeds
        Then polyopsonist wrt seeds are <expected_biomolecules>

        Examples:
        | organisms                                    | seeds              | expected_biomolecules    |
        | Com1Org1,Com1Org2,Com1Org3                   | M_E_c,M_F_c,M_X1_c | M_C_c:2                  |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4          | M_E_c,M_F_c,M_X1_c | M_C_c:2,M_X1_c:2         |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | M_E_c,M_F_c,M_X1_c | M_C_c:3,M_X1_c:2,M_N_c:2,M_F_c:2 |
        | Com1Org1,Com2Org4                            | M_E_c,M_F_c,M_X1_c | M_X1_c:2                 |
        | Com3Org1,Com3Org2,Com3Org3,Com3Org4          | M_E_c,M_F_c,M_X1_c | M_C_c:2,M_X1_c:2         |
        | Com3Org1,Com3Org2,Com3Org4                   | M_E_c,M_F_c,M_X1_c | M_X1_c:2                 |
        | Com3Org1,Com3Org2,Com3Org3                   | M_E_c,M_F_c,M_X1_c | M_C_c:2                  |
