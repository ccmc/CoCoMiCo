"""Report feature tests."""

from pytest import approx
from pytest_bdd import given, parsers, scenario, then, when

from cocomico import report, score
from cocomico.base import ExchangeNM, Seeds


@scenario("report_serialize.feature", "Serialization")
def test_serialization():
    """Serialization."""


@given(parsers.parse("{seed_biomolecules:S}"), target_fixture="seeds")
def _(seed_biomolecules):
    """<seeds>."""
    return Seeds(seed_biomolecules.split(","))


@when("serialize community", target_fixture="description")
def _(community):
    """serialize community."""
    return report.serialize_community(community)


@when("serialize results", target_fixture="results")
def _(community):
    """serialize results."""
    return report.serialize_results(community)[0]


@then("community and serialization agree")
def _(community, description):
    """community and serialization agree."""
    assert community.taxa == set(description["taxa"])

    files = set(str(path) for path in community.files.values())
    assert files == set(description["models"])


@then("community and serialized analyses agree")
def _(community, seeds, results):
    """community and serialization agree."""

    assert results["polyopsonist"] == community.polyopsonist(seeds)
    assert results["monopsonist"] == community.monopsonist(seeds)
    assert set(results["scope"]) == set(str(m) for m in community.scope(seeds))

    # Exchanges to compare are dicts; check for equality.
    all_exch = {
        k: ExchangeNM(
            producers=exch["producers"],
            consumers=exch["consumers"],
        )
        for k, exch in results["exchange"].items()
    }
    com_exch = {
        k: ExchangeNM(relations=relations)
        for k, relations in community.exchange(seeds).items()
    }
    for k in set(all_exch.keys()) | set(all_exch.keys()):
        assert all_exch[k] == com_exch[k]


@then("community and serialized subsets agree")
def _(community, seeds, results):
    """community and serialization agree."""
    assert set(results["activated"]) == set(str(m) for m in community.activated(seeds))
    assert set(results["produced_seeds"]) == community.produced_seeds(seeds)
    assert set(results["consumed_seeds"]) == community.consumed_seeds(seeds)


@then("community and serialized scores agree")
def _(community, seeds, results):
    """community and serialization agree."""
    competition = score.competition(community, seeds)
    cooperation = score.cooperation(community, seeds)
    delta = score.delta(community, seeds)
    rho = score.rho(community, seeds)

    assert results["competition"] == list(approx(x) for x in competition)
    assert results["cooperation"] == list(approx(x) for x in cooperation)
    assert results["delta"] == list(approx(x) for x in delta)
    assert results["rho"] == list(approx(x) for x in rho)
