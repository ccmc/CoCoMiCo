"""Sampling of communities feature tests."""

import json
from itertools import chain
from pathlib import Path
from typing import Iterable

from pytest_bdd import given, scenario, then, when

from sampling.name import Name
from sampling.types import Community as SampleCommunity
from sampling.types import NamedCommunity as SampleNamedCommunity
from sampling.write import write_specs


@scenario("sampling_write.feature", "Write sample")
def test_write_sample():
    """Write sample."""


@scenario("sampling_write.feature", "Write sample in parts")
def test_write_sample_in_parts():
    """Write sample in parts."""


@given("an ecosystem dict", target_fixture="ecosystem")
def _() -> Iterable[SampleNamedCommunity]:
    """an ecosystem dict."""
    ecos: Iterable[SampleNamedCommunity] = list(
        (
            Name(eco=f"eco{eco:02d}", size=10),
            [f"{(10 * eco + i):02d}.sbml" for i in range(10)],
        )
        for eco in range(5)
    )
    assert len(list(ecos)) == 5
    for eco, coms in ecos:
        assert str(eco).startswith("eco")
        assert all(int(str(com)[0:2]) // 10 == int(str(eco)[3:5]) for com in coms)

    return ecos


@given("output directory", target_fixture="output_path")
def _(tmp_path):
    """CSV output path."""
    return tmp_path / "specs"


@when("write specs in parts to a directory")
def _(ecosystem: Iterable[SampleNamedCommunity], output_path: Path):
    """write specs in parts to a directory."""
    write_specs(gen=ecosystem, out=output_path, eco="eco", models_per_spec=20)


@when("write specs to a directory")
def _(ecosystem: Iterable[SampleNamedCommunity], output_path: Path):
    """write specs to a directory."""
    write_specs(gen=ecosystem, out=output_path, eco="eco")


@then("combined specs equal the complete sample")
def _(ecosystem: Iterable[SampleNamedCommunity], output_path: Path):
    """combined specs equal the complete sample."""

    def combine_specs():
        """Read communities from all spec files."""
        for sf in output_path.glob("*.json"):
            with open(sf, "r", encoding="UTF-8") as f:
                yield json.load(f).items()

    ecos: dict[str, SampleCommunity] = {str(k): c for k, c in ecosystem}
    coms: dict[str, SampleCommunity] = dict(chain.from_iterable(combine_specs()))
    assert coms == ecos


@then("individual specs are parts of the sample")
def _(ecosystem: Iterable[SampleNamedCommunity], output_path: Path):
    """individual specs are parts of the sample."""
    ecos: dict[str, SampleCommunity] = {str(k): c for k, c in ecosystem}
    for sf in output_path.glob("*.json"):
        with open(sf, "r", encoding="UTF-8") as f:
            coms: dict[str, SampleCommunity] = json.load(f)
            assert coms.items() <= ecos.items()


@then("the spec equals the sample")
def _(ecosystem: Iterable[SampleNamedCommunity], output_path: Path):
    """the spec equals the sample."""
    ecos: dict[str, SampleCommunity] = {str(k): c for k, c in ecosystem}
    with open(output_path / "eco.json", "r", encoding="UTF-8") as f:
        coms: dict[str, SampleCommunity] = json.load(f)
    assert coms == ecos
