"""Community class feature tests."""

from pathlib import Path
from typing import List

from pytest_bdd import scenario, then, when

from cocomico.community import Community


@scenario("community_shared.feature", "Models can be shared between communities")
def test_models_can_be_shared_between_communities():
    """Models can be shared between communities."""


@when("create communities with reused models", target_fixture="communities")
def _(models: List[Path]) -> List[Community]:
    """create communities with reused models."""
    return [Community(models=models[i: (i + 3)]) for i in range(len(models))]


@then("communities are as expected")
def _(communities: List[Community], models: List[Path]):
    """communities are as expected."""
    assert len(communities) == len(models)

    all_communities = [(t, m) for c in communities for (t, m) in c.items()]
    assert len(all_communities) == 3 * (len(models) - 2) + 3
    assert len(set(t for t, _ in all_communities)) == len(models)


@then("reused models are shared")
def _(communities: List[Community]):
    """reused models are shared."""
    all_communities = [(t, m) for c in communities for (t, m) in c.items()]
    for taxon1, model1 in all_communities:
        for taxon2, model2 in all_communities:
            if taxon1 == taxon2:
                assert model1 is model2
            else:
                assert model1 is not model2
