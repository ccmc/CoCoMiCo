Feature: Community class
    Represent a microbial community

    Scenario Outline: Calculate activated
        Given community of organisms
        And useless seeds
        When we compute answers
        Then scope wrt seeds is empty
		And activated wrt seeds is empty
		And produced_seeds wrt seeds is empty
		And consumed_seeds wrt seeds is empty
		And metrics wrt seeds are zero
