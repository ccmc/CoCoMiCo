Feature: Sampling of communities
  Generate communities by sampling communities and ecosystems

  Scenario Outline: Sample from communities
    Given an ecosystem dict
    And a community list
    When sample communities equally
    And sample communities unequally
    Then samples have equally distributed origins
    And unequal samples have an extra
    And sample names are as expected

  Scenario Outline: Sample single communities
    Given an ecosystem dict
    When sample single communities
    Then samples have single origin

  Scenario Outline: Trivial sampling
    Given an ecosystem dict
    When sample ecosystems no sizes
    Then samples equal ecosystems

  Scenario Outline: Added value sample from communities
    Given an ecosystem dict
    When added value sample communities
    Then added value samples have all originals
    And added value samples have minus
    And added value samples have added

  Scenario Outline: Sample pairs from community spec
    Given a community definition file
    And SBML models directory
    When create community objects
    When sample pairs from community spec
    Then pairs sample has two members
    And pairs sample members are from community

  Scenario Outline: Sample using pool
    Given an ecosystem dict
    And a community list
    And a pool
    When sample communities unequally using pool
    Then the extras are from the pool

  Scenario Outline: Added value sample from community definition
    Given a community definition file
    And SBML models directory
    When convert community definition to ecosystem
    And added value sample communities
    Then added value samples have all originals
    And added value samples have minus
    And added value samples have added
    And added value samples have original names
