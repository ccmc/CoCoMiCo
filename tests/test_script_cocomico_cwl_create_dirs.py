"""CWL entrypoint feature tests."""

from os import getcwd
from pathlib import Path

from pytest_bdd import given, scenario, then, when


@scenario(
    "script_cocomico_cwl_create_dirs.feature",
    "Create output directory if needed for example",
)
def test_create_output_directory_if_needed_for_example():
    """Create output directory if needed for example."""


@scenario(
    "script_cocomico_cwl_create_dirs.feature",
    "Create output directory if needed for one community",
)
def test_create_output_directory_if_needed_for_one_community():
    """Create output directory if needed for one community."""


@given("a missing output directory path", target_fixture="output")
def _():
    """a missing output directory path."""
    return Path("missing_output_directory")


@given("a working directory", target_fixture="working_dir")
def _(tmp_path):
    """a working directory."""
    (working := tmp_path / "working").mkdir(parents=True, exist_ok=True)
    return working


@given("csv output path", target_fixture="output_csv")
def _(output):
    """csv output path."""
    return output / "example.csv"


@when("change to working directory")
def _(working_dir, monkeypatch):
    """change to working directory."""
    monkeypatch.chdir(working_dir)
    assert Path(getcwd()) == working_dir


@when("run cocomico for one community")
def _(seed_file, sbml_dir, models, output, output_csv, helpers):
    """run cocomico for one community."""
    # pylint: disable=R0913
    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        output=output,
        csv=output_csv,
        models=models,
    )
    assert result.exit_code == 0


@then("output directory is created")
def _(output):
    """output directory is created."""
    assert output.is_dir()


@then("have result files")
def _(output):
    """have result files."""
    assert output.is_dir()
    assert (output / "example.csv").is_file()
