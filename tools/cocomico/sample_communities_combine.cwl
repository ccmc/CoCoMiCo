cwlVersion: v1.2
class: Workflow

label: sample_communities_combine
id: sample_communities_combine

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}

inputs:
  filename: string?
  dirname: string?
  sources:
    type:
      type: array
      items:
        - File
        - Directory
  tasks:
    type:
      type: array
      items:
        type: record
        fields:
          sources: string[]
          dest: string
          name: string?
          reps: int?
          added_value: boolean?
          size: int[]?
          pool: Directory?
  
outputs:
  combined:
    type: File
    outputSource: combine_json/json
  combined_dir:
    type: Directory
    outputSource: combine_dir/dir
  sample:
    type: File[]
    outputSource: sampling/sample

steps:
  
  sampling:
    run: sample_communities_chain.cwl
    in:
      sources:
        source: sources
      tasks:
        source: tasks
    out: [sample, sample_dir]

  combine_json:
    run:
      class: CommandLineTool
      baseCommand: ["jq", "-s", "add"]
      inputs:
        file_name:
          type: string
          default: "combined.json"
        file_list:
          type: File[]
          inputBinding:
            position: 1
      outputs:
        json:
          type: File
          streamable: true
          outputBinding: {glob: "$(inputs.file_name)"}
      stdout: $(inputs.file_name)

    in:
      file_list:
        source: sampling/sample
      file_name:
        source: filename
    out: [json]

  combine_dir:
    run:
      class: ExpressionTool
      inputs:
        dir_name:
          type: string
          default: "combined_d"
        dir_list: Directory[]
      outputs:
        dir: Directory

      expression: |
        ${
          return {
            "dir": {
              "class": "Directory",
              "basename": inputs.dir_name,
              "listing": inputs.dir_list
            }
          }
        }

    in:
      dir_list:
        source: sampling/sample_dir
      dir_name:
        source: dirname
    out: [dir]


doc: |
  Usage: sample_communities_combine.cwl sample_tasks_job.yaml

    Run in order a sequence of community sampling tasks. Each task is
    defined using an input record for sample_communities.cwl, and may
    refer to the results of previous tasks. The results are combined
    into a single JSON community specification, and a single directory
    of sample ecosystem directories.

  Inputs:
    sources           Source ecosystems to make available as inputs
    tasks             List of sampling tasks
    models-per-spec   Split output specs <= N models
    random-seed       Random generator seed
    random-state      Pickled random generator state
    loglevel          Logging level INFO, DEBUG, WARN

s:author:
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005

s:codeRepository: https://gitlab.inria.fr/CCMC/CoCoMiCo.git
s:license: https://spdx.org/licenses/GPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2023-08-29"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.25.owl
