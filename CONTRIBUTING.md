# Contributing

CoCoMiCo is research software developed by the Pleiade
project-team. Anyone may contribute but please use common
sense and try to follow these basic rules.

## Testing environment

[Tox](https://tox.wiki/en/stable/) environments are used for testing
and generation of virtual environments. After any modification to the
source code, run the default set of tests using

```sh
tox run
```

Complete tests are [run in CI](https://gitlab.inria.fr/ccmc/CoCoMiCo/-/pipelines)
for every branch or merge request.

## Issues

Feel free to open issues but please try to keep individual issues
focused on a single question, problem, or enhancement.
The project has a basic set of 
[issue labels](https://gitlab.inria.fr/ccmc/CoCoMiCo/-/labels). 
The **bug**, **enhancement**, and **documentation** labels
are particularly helpful. It is fine to create speculative issues.
Please only mark issues as **critical** if your project is stuck
until resolution.

Every bug report needs a minimal reproducible test case.

When reporting a bug or suggesting an enhancement, strongly consider
describing it in natural language in the form of a feature using 
[Gherkin](https://cucumber.io/docs/gherkin/reference/) syntax.

## Merge requests

Any time you start to work on an issue, please let the team know by
opening a merge request. If a merge request is already open, use the
comments to contribute if you can; only open a second merge request
when your contribution involves writing code that and it can't be included in
incorporated into the open merge request.

When reviewing a merge request, use inline comments on the source code
whenever possible.

Code reviews are not mandatory but are generally a good idea.

## Code quality

All branches and merge requests will run through CI testing, which
includes code quality gates and Sonarqube analysis. Please respect
these gates. 

All source code should be processed using `isort` and `black` to
ensure consistent formatting.

This is library code and extra effort should be expended to guarantee
correctness. Python type hints should be used systematically, and any
ambiguities or errors flagged by Mypy should be corrected. In truly
exceptional cases, flagged code can be excluded on a case-by-case
basis.

## Versioing

We use semantic versioning so any changes to the software interface
need to bump the appropriate version.
