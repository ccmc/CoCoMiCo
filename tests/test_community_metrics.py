"""Community feature tests."""

from pytest import approx
from pytest_bdd import parsers, scenario, then, when

from cocomico import score
from cocomico.constants import COMP_NUM, COOP_CONS, COOP_NUM, COOP_PROD


@scenario("community_metrics.feature", "Compute metrics")
def test_compute_metrics():
    """Compute metrics."""


@when("we compute metrics")
def we_compute_metrics():
    """we compute metrics."""


@then(
    parsers.parse(
        "each {metrics:S} has the expected "
        "{coop:g} {coop_num:g} {coop_prod:g} {coop_cons:g} {comp:g} {comp_num:g}"
    )
)
def _(community, seeds, coop, coop_num, coop_prod, coop_cons, comp, comp_num):
    """each <organisms> has the expected <coop> <coop_num> <coop_prod>
    <coop_cons> <comp> <comp_num>."""
    # pylint: disable=R0913
    computed_coop, computed_coop_metrics = score.cooperation(community, seeds)
    assert coop == approx(computed_coop, 0.01)
    assert coop_num == computed_coop_metrics[COOP_NUM]
    assert coop_prod == approx(computed_coop_metrics[COOP_PROD], 0.01)
    assert coop_cons == approx(computed_coop_metrics[COOP_CONS], 0.01)
    computed_comp, computed_comp_metrics = score.competition(community, seeds)
    assert comp == approx(computed_comp, 0.01)
    assert comp_num == approx(computed_comp_metrics[COMP_NUM])
