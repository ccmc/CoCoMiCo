"""CWL entrypoint feature tests."""

from pathlib import Path

from click.testing import CliRunner
from pytest_bdd import given, scenario, then, when

from cocomico import cwl, report


@scenario("script_cocomico_cwl_ignore_seed_reactions.feature", "Ignore seed reactions")
def test_ignore_seed_reactions():
    """Ignore seed reactions."""


@given("empty seeds", target_fixture="empty_seed_file")
def _():
    """empty seeds."""
    return (Path("tests") / "data_test" / "seeds_d" / "seeds_empty.sbml").resolve()


@given("SBML noexchange directory", target_fixture="sbml_dir_noexch")
def _() -> Path:
    """SBML noexchange directory."""
    return (Path("tests") / "data_test" / "sbml_noexch").resolve()


@given("tabular output path", target_fixture="output_csv")
def _(tmp_path):
    """tabular output path."""
    return tmp_path / "results.csv"


@when("run cocomico ignoring seed reactions")
def _(empty_seed_file, sbml_dir_noexch, output_csv):
    """run cocomico ignoring seed reactions."""
    # pylint: disable=R0913
    args = [
        "--ignore-seed-reactions",
        "community",
        "--seeds-file",
        empty_seed_file,
        "--sbml-dir",
        str(sbml_dir_noexch),
        "--csv-output",
        str(output_csv),
        "--no-header-csv",
    ]
    runner = CliRunner()
    result = runner.invoke(cwl.entrypoint, args)
    assert result.exit_code == 0
    assert output_csv.stat().st_size > 0


@then("tabular file has no exchanges")
def _(output_csv):
    """tabular file has no exchanges."""
    table = report.read_tabular(output_csv)
    for row in table.values():
        assert float(row[2]) == 0.0  # coop
        assert float(row[4]) == 0.0  # comp
