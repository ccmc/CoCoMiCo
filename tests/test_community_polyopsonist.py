"""Community class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Seeds
from cocomico.community import Community


@scenario("community_polyopsonist.feature", "Identify polyopsonist metabolites")
def test_identify_polyopsonist_metabolites():
    """Identify polyopsonist metabolites."""


@given(parsers.parse("{seeds:S}"), target_fixture="seeds")
def _(seeds: str) -> Seeds:
    """<seeds>."""
    return Seeds([Biomolecule(s) for s in seeds.split(",")])


@when("compute polyopsonist wrt seeds", target_fixture="polyopsonist")
def compute_polyopsonist_wrt_seeds(community: Community, seeds: Seeds):
    """compute polyopsonist wrt seeds."""
    return community.polyopsonist(seeds)


@then(parsers.parse("polyopsonist wrt seeds are {expected_biomolecules:S}"))
def polyopsonist_wrt_seeds_are_expected_biomolecules(
    polyopsonist, expected_biomolecules
):
    """polyopsonist wrt seeds are <expected_biomolecules>."""
    expected = {
        Biomolecule(pair[0]): int(pair[1])
        for ex in expected_biomolecules.split(",")
        if (pair := ex.split(":"))[0]
    }
    assert polyopsonist == expected
