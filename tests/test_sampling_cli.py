"""Sampling of communities feature tests."""

import json
import logging
import re
from itertools import chain
from pathlib import Path
from typing import Iterable

from click.testing import CliRunner
from pytest_bdd import given, parsers, scenario, then, when

from sampling.cwl import JSON, PICKLE, main
from sampling.types import Community


@scenario("sampling_cli.feature", "Command line with name")
def test_command_line_with_name():
    """Command line with name."""


@scenario("sampling_cli.feature", "Command line without name")
def test_command_line_without_name():
    """Command line without name."""


@scenario("sampling_cli.feature", "Command line from JSON spec")
def test_command_line_from_json_spec():
    """Command line from JSON spec."""


@scenario("sampling_cli.feature", "Generate added value samples")
def test_generate_added_value_samples():
    """Generate added value samples."""


@scenario("sampling_cli.feature", "Reproducibility from random seed")
def test_reproducibility_from_random_seed():
    """Reproducibility from random seed."""


@scenario("sampling_cli.feature", "Reproducibility from random state")
def test_reproducibility_from_random_state():
    """Reproducibility from random state."""


@scenario("sampling_cli.feature", "Split output")
def test_split_output():
    """Split output."""


@scenario("sampling_cli.feature", "No sources")
def test_no_sources():
    """No sources."""


@scenario("sampling_cli.feature", "Model file source forces anonymous")
def test_model_file_source_forces_anonymous():
    """Model file source forces anonymous."""


@scenario("sampling_cli.feature", "Generate using pool")
def test_generate_using_pool():
    """Generate using pool."""


@scenario("sampling_cli.feature", "Sources as args or options yield the same")
def test_sources_as_args_or_options_yield_the_same():
    """Sources as args or options yield the same."""


@scenario("sampling_cli.feature", "Chain sampling")
def test_chain_sampling():
    """Chain sampling."""


@given("output directory", target_fixture="output_path")
def _(tmp_path):
    """spec output path."""
    return tmp_path / "specs"


@given("source is sbml directory", target_fixture="source")
def _(sbml_dir):
    """source is sbml directory."""
    return sbml_dir


@given("source is community definition file", target_fixture="source")
def _(community_json):
    """source is community definition file."""
    return community_json


@given("random seed", target_fixture="random_seed")
def _():
    """random seed."""
    return 202404291400


@given("random state", target_fixture="random_state")
def _():
    """random state."""
    return (Path("tests") / "data_test" / "reproducibility").resolve()


@given("a pool", target_fixture="pool")
def _() -> Path:
    """a pool."""
    return (Path("tests") / "data_test" / "sbml_nonstd").resolve()


@given("community definition files", target_fixture="community_json_s")
def _() -> list[Path]:
    """community definition files."""
    return [
        f.resolve()
        for f in (Path("tests") / "data_test" / "specs_d").glob(f"spec_*{JSON}")
    ]


@when(
    parsers.parse("generate named samples {name:S} {sizes:S} {reps:d}"),
    target_fixture="name",
)
def _(name: str, sizes: str, reps: int, output_path: Path, source: Path):
    """generate named samples <name> <sizes> <reps>."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", str(reps), str(source)]
    args_sizes = [arg for s in sizes.split(",") for arg in ["--size", s]]

    result = runner.invoke(main, ["sample", "--name", name, *args_sizes, *args])
    assert result.exit_code == 0
    return name


@when(parsers.parse("generate unnamed samples {sizes:S} {reps:d}"))
def _(sizes: str, reps: int, output_path: Path, source: Path):
    """generate unnamed samples <sizes> <reps>."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", str(reps), str(source)]
    args_sizes = [arg for s in sizes.split(",") for arg in ["--size", s]]

    result = runner.invoke(main, ["sample", *args_sizes, *args])
    assert result.exit_code == 0


@when(
    parsers.parse("generate added value samples {name:S} {sizes:S} {reps:d}"),
    target_fixture="name",
)
def _(name: str, sizes: str, reps: int, output_path: Path, source: Path):
    """generate added value samples <name> <sizes> <reps>."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", str(reps), str(source)]
    args_sizes = [arg for s in sizes.split(",") for arg in ["--size", str(s)]]

    result = runner.invoke(
        main, ["sample", "--added-value", "--name", name, *args_sizes, *args]
    )
    assert result.exit_code == 0
    return name


@when("generate single sample with random seed")
def _(random_seed: int, output_path: Path, community_json: Path):
    """generate single sample with random seed."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", "1", str(community_json)]
    args_sizes = [arg for s in [2] for arg in ["--size", str(s)]]
    args_random = ["--random-seed", str(random_seed)]

    result = runner.invoke(main, [*args_random, "sample", *args_sizes, *args])
    assert result.exit_code == 0


@when("generate single sample with random state")
def _(random_state: Path, output_path: Path, community_json: Path):
    """generate single sample with random state."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", "1", str(community_json)]
    args_sizes = [arg for s in [2] for arg in ["--size", str(s)]]
    args_random = ["--random-state", str(random_state)]

    result = runner.invoke(main, [*args_random, "sample", *args_sizes, *args])
    assert result.exit_code == 0


@when("generate 100 pairs with max 50 per sample")
def _(output_path: Path, community_json: Path):
    """generate 100 pairs with max 50 per sample."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", "100", str(community_json)]
    args_split = ["--models-per-spec", "50"]
    args_sizes = [arg for s in [2] for arg in ["--size", str(s)]]

    result = runner.invoke(main, [*args_split, "sample", *args_sizes, *args])
    assert result.exit_code == 0


@when("invoke with no sources")
def _(output_path: Path):
    """invoke with no sources."""
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", "100", "--size", "2"]

    result = runner.invoke(main, ["sample", *args])
    assert result.exit_code == 0


@when("generate from source having model files")
def _(output_path: Path, community_json: Path, models: list[Path], caplog):
    """generate from source having model files."""
    caplog.set_level(logging.DEBUG)
    runner = CliRunner()
    args = ["--dest", str(output_path), str(community_json), str(models[0])]

    result = runner.invoke(main, ["sample", *args])
    assert result.exit_code == 0


@when("read the sample from default name", target_fixture="sample")
def _(output_path: Path) -> dict[str, Community]:
    """read the sample from default name."""

    def combine_specs() -> Iterable[dict]:
        """Read communities from all spec files."""
        for sf in output_path.glob(f"*{JSON}"):
            with open(sf, "r", encoding="UTF-8") as f:
                yield json.load(f).items()

    sample: dict[str, Community] = dict(chain.from_iterable(combine_specs()))
    return sample


@when("generate unnamed samples using pool")
def _(output_path: Path, source: Path, pool: Path):
    """generate unnamed samples using pool."""
    sizes = [5]  # 1 from each of 4 sources, 1 from pool
    reps = 10
    runner = CliRunner()
    args = ["--dest", str(output_path), "--reps", str(reps), str(source)]
    args_sizes = [arg for s in sizes for arg in ["--size", str(s)]]
    args_pool = ["--pool", str(pool)]

    result = runner.invoke(main, ["sample", *args_sizes, *args_pool, *args])
    assert result.exit_code == 0


@when("read the sample from name.json", target_fixture="sample")
def _(output_path: Path, name) -> dict[str, Community]:
    """read the sample from name.json."""
    with open(output_path / f"{name}{JSON}", "r", encoding="UTF-8") as f:
        sample: dict[str, Community] = json.load(f)
    return sample


@when("generate trivial samples using source args", target_fixture="from_src_args")
def _(output_path: Path, community_json_s: list[Path]) -> Path:
    """generate trivial samples using source args."""
    runner = CliRunner()
    dest = output_path / "from_args"
    args = ["--dest", str(dest)]
    sources = [x for f in community_json_s for x in ["--source", str(f)]]

    result = runner.invoke(main, ["sample", *args, *sources])
    assert result.exit_code == 0
    return dest / f"eco{JSON}"


@when("generate trivial samples using source options", target_fixture="from_src_opts")
def _(output_path: Path, community_json_s: list[Path]) -> Path:
    """generate trivial samples using source args."""
    runner = CliRunner()
    dest = output_path / "from_args"
    args = ["--dest", str(dest)]
    sources = [str(f) for f in community_json_s]

    result = runner.invoke(main, ["sample", *args, *sources])
    assert result.exit_code == 0
    return dest / f"eco{JSON}"


@when("chain generate samples", target_fixture="from_src_opts")
def _(output_path: Path, community_json_s: list[Path]) -> Path:
    """chain generate samples."""
    runner = CliRunner()

    # Tuples (source, dest)
    coms = [(c, Path(f"dest{c.stem.split('_')[1]}")) for c in community_json_s]
    assert all(c.exists() for c in community_json_s)

    # Each sample: dest from source + previous dest if exists
    samples = [
        list(["--dest", output_path / c[1], "--source", c[0]])
        + ([] if i < 1 else ["--source", output_path / str(coms[i - 1][1])])
        for i, c in enumerate(coms)
    ]

    # All sample-chain tasks
    args = [str(x) for s in samples for x in ["sample-chain"] + s]

    result = runner.invoke(main, args)
    assert result.exit_code == 0
    return output_path / coms[-1][1]


@then(parsers.parse("the id pattern matches {id_pattern:S}"))
def _(sample: dict[str, Community], id_pattern: str):
    """the id pattern matches <id_pattern>."""
    r = re.compile(id_pattern)
    for k in sample.keys():
        # Instead of all(), use separate assert for each so we see the exact failure.
        assert re.match(r, k)


@then(parsers.parse("there are {n_com:d} communities"))
def _(sample: dict[str, Community], n_com: int):
    """there are <n_com> communities."""
    assert n_com == len(sample)


@then(parsers.parse("there are {n_mod:d} total models"))
def _(sample: dict[str, Community], n_mod: int):
    """there are <n_mod> total models."""
    assert n_mod == sum(len(c) for c in sample.values())


@then(parsers.parse("there are {n_minus:d} minus communities"))
def _(sample: dict[str, Community], n_minus: int):
    """there are <n_minus> minus communities."""
    assert n_minus == sum(1 for n in sample.keys() if n.split("_")[2] == "minus")


@then(parsers.parse("there are {n_added:d} added communities"))
def _(sample: dict[str, Community], n_added: int):
    """there are <n_added> added communities."""
    assert n_added == sum(1 for n in sample.keys() if n.split("_")[2] == "added")


@then("the sample has expected members")
def _(sample: dict[str, Community]):
    """the sample has expected members."""
    com = list(sample.values())[0]
    assert set(com) == {"Com1Org2.sbml", "Com2Org4.sbml"}


@then("there are 100 samples in 4 files")
def _(output_path: Path, sample: dict[str, Community]):
    """there are 100 samples in 4 files."""
    assert len(sample) == 100
    assert len(list(output_path.glob(f"*{JSON}"))) == 4


@then("nothing is done")
def _(output_path: Path):
    """nothing is done."""
    assert len(list(output_path.glob(f"*{JSON}"))) == 0
    assert len(list(output_path.glob(f"*{PICKLE}"))) == 0


@then("log shows source was anonymous")
def _(caplog):
    """log shows source was anonymous."""
    assert "returning list of anonymous single community" in caplog.text


@then("the extras are from the pool")
def _(sample: dict[str, Community]):
    """the extras are from the pool."""
    for com in sample.values():
        assert sum(1 if "nonstd" in str(m) else 0 for m in com) == 1


@then("the samples from both source kinds are the same")
def _(from_src_args: Path, from_src_opts: Path):
    """the samples from both source kinds are the same."""
    with (
        open(from_src_args, "r", encoding="UTF-8") as farg,
        open(from_src_opts, "r", encoding="UTF-8") as fopt,
    ):
        sample_from_args: dict[str, Community] = json.load(farg)
        sample_from_opts: dict[str, Community] = json.load(fopt)
    assert sample_from_args == sample_from_opts


@then("all chained samples are found")
def _(from_src_opts: Path, community_json_s: list[Path], output_path: Path):
    """all chained samples are found."""
    assert from_src_opts.exists()
    coms = [(c, Path(f"dest{c.stem.split('_')[1]}")) for c in community_json_s]
    for i, (_, dest) in enumerate(coms):
        assert (p := output_path / dest).exists() and p.is_dir()
        if i > 0:
            with open(p / f"eco{JSON}", "r", encoding="UTF-8") as f:
                sample = json.load(f)
                assert any("dest" in k for k in sample.keys())
