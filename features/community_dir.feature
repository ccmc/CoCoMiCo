Feature: Community class
  Represent a microbial community

  Scenario Outline: Initialize a community from a directory
    Given SBML models directory
    When create community object from directory
    Then the community has expected models
