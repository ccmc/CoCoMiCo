"""Seeds class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Seeds


@scenario("seeds_init.feature", "Initialize seeds")
def test_initialize_seeds():
    """Initialize seeds."""


@scenario("seeds_init.feature", "Initialize seeds using kwarg")
def test_initialize_seeds_using_kwarg():
    """Initialize seeds using kwarg."""


@scenario("seeds_init.feature", "Initialize named seeds")
def test_initialize_named_seeds():
    """Initialize named seeds."""


@given(
    parsers.parse("a set of {metabolite_biomolecules:S}"),
    target_fixture="metabolite_biomolecules",
)
def _(metabolite_biomolecules) -> list[Biomolecule]:
    """a set of <metabolite_biomolecules>."""
    return [Biomolecule(i) for i in metabolite_biomolecules.split(",")]


@given(
    parsers.parse("a metabolite list {metabolite_list:S}"),
    target_fixture="metabolite_list",
)
def _(metabolite_list) -> list[str]:
    """a metabolite list <metabolite_list>."""
    return metabolite_list.split(",")


@given(
    parsers.parse("a name {name:S}"),
    target_fixture="name",
)
def _(name) -> str:
    """a name <name>."""
    return name


@when("create anonymous seeds object", target_fixture="seeds")
def _(metabolite_biomolecules) -> Seeds:
    """create anonymous seeds object."""
    return Seeds(metabolite_biomolecules)


@when("create anonymous seeds object using kwarg", target_fixture="seeds")
def _(metabolite_biomolecules) -> Seeds:
    """create anonymous seeds object using kwarg."""
    return Seeds(seeds=metabolite_biomolecules)


@when("create named seeds object", target_fixture="named_seeds")
def _(metabolite_biomolecules, name) -> Seeds:
    """create named seeds object."""
    return Seeds(seeds=metabolite_biomolecules, name=name)


@then("anon object contains expected metabolites")
def _(seeds: Seeds, metabolite_list: list[str]):
    """anon object contains expected metabolites."""
    assert set(metabolite_list) == seeds


@then("named object contains expected metabolites")
def _(named_seeds: Seeds, metabolite_list: list[str]):
    """anon object contains expected metabolites."""
    assert set(metabolite_list) == named_seeds


@then("named object has expected name")
def _(named_seeds: Seeds, name: str):
    """object contains expected name."""
    assert named_seeds.name == name
