"""Community class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, MetaboliteSet, Seeds
from cocomico.community import Community


@scenario("community_scope.feature", "Calculate scope")
def test_calculate_scope():
    """Calculate scope."""


@given(parsers.parse("{seeds:S}"), target_fixture="seeds")
def _(seeds: str) -> Seeds:
    """<seeds>."""
    return Seeds([Biomolecule(s) for s in seeds.split(",")])


@when("compute scope wrt seeds", target_fixture="scope")
def _(community: Community, seeds: Seeds) -> MetaboliteSet:
    """compute scope wrt seeds."""
    return community.scope(seeds)


@then(parsers.parse("scope wrt seeds is {expected_scope:S}"))
def _(scope: MetaboliteSet, expected_scope: str):
    """scope wrt seeds is <expected_scope>."""
    expected = MetaboliteSet(
        {Metabolite.from_string(s) for s in expected_scope.split(",")}
    )
    assert scope == expected


@then("taxa of scope are from the community")
def _(community, scope):
    """taxa of scope are from the community."""
    assert scope.taxa == community.taxa


@then("seeds are now in community answers")
def _(community, seeds):
    """taxa of scope are from the community."""
    assert seeds in community.seeds()
