"""Community class feature tests."""

from pytest_bdd import given, scenario, then, when

from cocomico.community import Community
from cocomico.model import Model


@scenario("community_merge.feature", "Merge models into a community")
def test_merge_models_into_a_community():
    """Merge models into a community."""


@given("a multi-model community", target_fixture="original")
def _(sbml_dir):
    """a multi-model community."""
    original = Community(sbml_dir=sbml_dir)
    files = [str(f.stem) for f in sbml_dir.iterdir()]
    assert set(original.keys()) == set(files)
    assert len(original.models) == len(files)
    return original


@when("merge models into a new community", target_fixture="merged")
def _(original):
    """merge models into a new community."""
    models = {
        taxon: Model(relations=model.relations, biomolecule=model.biomolecule)
        for taxon, model in original.models.items()
    }
    merged = Community(merge=models)
    return merged


@then("the merged community has the same properties")
def _(original, merged):
    """the merged community has the same properties."""
    assert original.taxa == merged.taxa
    assert original.biomolecule == merged.biomolecule
    assert original.products == merged.products
    assert original.reactants == merged.reactants
    assert original.reactions == merged.reactions


@then("the merged community has the same models")
def _(original, merged):
    """the merged community has the same models."""
    for role in ["product", "reactant"]:
        for taxon in original.taxa:
            original_relations = set(original.models[taxon].relations[role])
            merged_relations = set(merged.models[taxon].relations[role])
            assert original_relations == merged_relations
