Feature: Community class
  Represent a microbial community

  Scenario Outline: Get LP answers
    Given SBML models directory
    And seeds
    When create community object
    Then lp_fact contain expected keys
    And answers for given seeds contain expected scope
    And answers for given seeds contain expected exchanges
    And exchanges are cartesian product
