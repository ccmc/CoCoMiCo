"""Community class feature tests."""

from pytest_bdd import parsers, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, MetaboliteSet, Seeds, Taxon
from cocomico.community import Community


@scenario("community_scope_bycommunity.feature", "Calculate scope by community")
def test_calculate_scope():
    """Calculate scope by community."""


@when(parsers.parse("choose {individual:S}"), target_fixture="individual")
def _(individual) -> Taxon:
    """choose <individual>."""
    return Taxon(individual)


@when("compute scope wrt seeds for individual", target_fixture="scope")
def _(community: Community, seeds: Seeds, individual: Taxon) -> MetaboliteSet:
    """compute scope wrt seeds for individual."""
    return community.scope(seeds, choice=individual)


@then(parsers.parse("scope wrt seeds for individual is {expected_scope:S}"))
def _(individual: Taxon, expected_scope: str, scope: MetaboliteSet):
    """scope wrt seeds for individual is <expected_scope>."""
    # Compare observed and expected metabolites
    expected = MetaboliteSet(
        {
            Metabolite(biomolecule=Biomolecule(m), provenance=Taxon(individual))
            for m in expected_scope.split(",")
        }
    )
    observed = scope
    assert observed == expected

    # Check that taxa of activated are the same taxon
    for m in observed:
        assert m.provenance == individual
