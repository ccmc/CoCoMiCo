"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Taxon
from cocomico.community import Community


@scenario("community_exchange.feature", "Identify exchanged metabolites")
def test_identify_exchanged_metabolites():
    """Identify exchanged metabolites."""


@given(parsers.parse("a choice of {community:S}"), target_fixture="community")
def a_choice_of_community(community):
    """a choice of <community>."""
    return community


@given("community objects", target_fixture="communities")
def _(community_json: Path, sbml_dir: Path) -> dict[str, Community]:
    """community objects."""
    return Community.from_specification(spec=community_json, sbml_dir=sbml_dir)


@given(parsers.parse("a {metabolite:S} biomolecule"), target_fixture="metabolite")
def a_metabolite_biomolecule(metabolite):
    """a <metabolite> biomolecule."""
    return Biomolecule(metabolite)


@when("compute exchanged", target_fixture="exchange")
def compute_exchanged(communities, community, seeds):
    """compute exchanged."""
    return communities[community].exchange(seeds)


@then(parsers.parse("have expected {pairs} of exchanged (producer, consumer) taxa"))
def have_expected_pairs_of_exchanged_producer_consumer_taxa(
    exchange, metabolite, pairs
):
    """have expected <pairs> of exchanged (producer, consumer) taxa."""
    assert metabolite in exchange
    for pair in pairs.split(","):
        i, j = pair.split("-")
        assert (Taxon(i), Taxon(j)) in exchange[metabolite]
