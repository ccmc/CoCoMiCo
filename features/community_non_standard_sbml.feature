Feature: Community class
  Represent a microbial community

  Scenario Outline: Handle SBML no namespace
    Given SBML models directory
    When create community object from no_ns
    Then no_ns model taxon is as expected

  Scenario Outline: Handle SBML no id
    Given SBML models directory
    When create community object from no_id
    Then no_id model taxon is the file stem

  Scenario Outline: Handle SBML no model
    Given SBML models directory
    When create community object from no_model
    Then no_model community is empty

  Scenario Outline: Handle SBML no sbml
    Given SBML models directory
    When create community object from no_sbml
    Then no_sbml community is empty

  Scenario Outline: Handle SBML parse error
    Given SBML models directory
    When create community object from invalid expecting exception
    Then exception was raised

  Scenario Outline: Handle SBML not relative
    Given SBML models directory
    When create community object not relative to sbml_dir
    Then path is not relativized

  Scenario Outline: Handle SBML no sbml_dir
    Given SBML models directory
    When create community object without sbml_dir
    Then path is not relativized
