#!/usr/bin/env cwl-runner
class: CommandLineTool
cwlVersion: v1.2

id: "sample_communities"
label: "sample_communities"

hints:
  - class: DockerRequirement
    dockerPull: quay.io/biocontainers/python:3.10.4
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.sources)  # allow relative paths

inputs:
  sources:
    type:
      type: array
      items:
        - File
        - Directory
  dests: string[]?
  tasks:
    inputBinding:
      position: 5
    type:
      type: array
      items:
        type: record
        fields:
          sources: string[]
          dest: string?
          name: string?
          reps: int?
          added_value: boolean?
          size: int[]?
          pool: Directory?
      inputBinding:
        valueFrom: |
          ${
            var task = []
            var dests = inputs.dests ? inputs.dests : (self.dest ? [self.dest] : [])
            for (var di=0; di<dests.length; di++) {
              task = task.concat("sample-chain")
              task = task.concat(["--dest", self.dest ? self.dest : dests[di]])
              task = self.name ? task.concat(["--name", self.name]) : task
              task = self.reps ? task.concat(["--reps", self.reps]) : task
              task = self.added_value ? task.concat(["--added-value"]) : task
              task = self.pool ? task.concat(["--pool", self.pool]) : task
              if (self.size) {
                var sizes = []
                for (var i=0; i<self.size.length; i++) {
                  sizes = sizes.concat(["--size", self.size[i]])
                }
                task = task.concat(sizes)
              }
              if (self.sources) {
                var sources = []
                for (var i=0; i<self.sources.length; i++) {
                  sources = sources.concat(["--source", self.sources[i]])
                }
                task = task.concat(sources)
              }
            }
            return task
          }
  random-seed:
    type: int?
    doc: |
      Random generator seed
    inputBinding:
      position: 2
      prefix: "--random-seed"
  models-per-spec:
    type: int?
    doc: |
      Split output specs ≤ N models
    inputBinding:
      position: 2
      prefix: "--models-per-spec"
  random-state:
    type: File?
    doc: |
      Pickled random generator state
    inputBinding:
      position: 2
      prefix: "--random-state"
  loglevel:
    type:
      type: enum
      symbols: ["DEBUG", "INFO", "WARN"]
    doc: |
      Logging level INFO, DEBUG, WARN
    default: INFO
    inputBinding:
      position: 2
      prefix: "--loglevel"

outputs:
  sample:
    type: File[]
    doc: |
      Complete dest directory with all generated JSON outputs
    outputBinding:
      glob: "$(runtime.outdir)/*"
      outputEval: |
        ${
          var fileglob = /.*\.json/;
          var dests = inputs.tasks.map(function(t){ return t.dest }) + inputs.dests
          var sample_d = self.filter(function(d){return d.class=="Directory" && dests.indexOf(d.basename) != -1 })
          var sample_n = sample_d.map(function(d){return d.listing ? d.listing.filter(function(f){return f.basename.match(fileglob)}) : []})
          var sample = sample_n.reduce(function(acc, curr){return acc.concat(curr)}, []);
          return sample
        }
  sample_dir:
    type: Directory[]
    doc: |
      Complete dest directory with all generated JSON outputs
    outputBinding:
      glob: "$(runtime.outdir)/*"
      outputEval: |
        ${
          var dests = inputs.tasks.map(function(t){ return t.dest }) + inputs.dests
          var sample_d = self.filter(function(d){return d.class=="Directory" && dests.indexOf(d.basename) != -1 })
          return sample_d
        }
  standard_output:
    type: stdout
  standard_error:
    type: stderr

baseCommand:
  - "sample-communities"

stdout: stdout.txt
stderr: stderr.txt


doc: |
  Usage: sample_communities_chain.cwl sample_tasks_job.yaml

    Run in order a sequence of community sampling tasks. Each task is
    defined using an input record for sample_communities.cwl, and may
    refer to the results of previous tasks.

  Inputs:
    sources           Source ecosystems to make available as inputs
    tasks             List of sampling tasks
    models-per-spec   Split output specs <= N models
    random-seed       Random generator seed
    random-state      Pickled random generator state
    loglevel          Logging level INFO, DEBUG, WARN

s:author:
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005

s:codeRepository: https://gitlab.inria.fr/CCMC/CoCoMiCo.git
s:license: https://spdx.org/licenses/GPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2023-08-29"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.25.owl
