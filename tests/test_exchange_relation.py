"""Exchange class feature tests."""

from pytest_bdd import given, scenario, then, when

from cocomico.base import ExchangeNM, Taxon


@scenario("exchange_relation.feature", "Create exchanges and iterate over them")
def test_create_exchanges_and_iterate_over_them():
    """Create exchanges and iterate over them."""


@given("a collection of taxa", target_fixture="taxa")
def _() -> list[Taxon]:
    """a collection of input taxa."""
    return [Taxon(i) for i in ["A", "B", "C", "D", "E", "F"]]


@given("a collection of input tuples", target_fixture="tuples")
def _(taxa) -> set[tuple[Taxon, Taxon]]:
    """a collection of input tuples."""
    return {
        (taxa[0], taxa[4]),
        (taxa[0], taxa[5]),
        (taxa[1], taxa[3]),
        (taxa[2], taxa[5]),
    }


@when("create an exchange from tuples", target_fixture="exch_tuples")
def _(tuples) -> ExchangeNM:
    """create an exchange from tuples."""
    return ExchangeNM(relations=tuples)


@when("create an exchange from taxa", target_fixture="exch_taxa")
def _(tuples) -> ExchangeNM:
    """create an exchange from taxa."""
    p = {p for p, _ in tuples}
    c = {c for _, c in tuples}
    return ExchangeNM(producers=p, consumers=c)


@then("input tuples are in the exchanges")
def _(tuples, exch_tuples, exch_taxa):
    """input tuples are in the exchanges."""
    for producer_consumer in tuples:
        assert producer_consumer in exch_tuples
        assert producer_consumer in exch_taxa


@then("iterating produces the expected tuples")
def _(exch_tuples, taxa):
    """iterating produces the expected tuples."""
    # expand set from iterator
    all_relations = {*exch_tuples}
    assert all_relations == {
        (taxa[0], taxa[3]),
        (taxa[1], taxa[3]),
        (taxa[2], taxa[3]),
        (taxa[0], taxa[4]),
        (taxa[1], taxa[4]),
        (taxa[2], taxa[4]),
        (taxa[0], taxa[5]),
        (taxa[1], taxa[5]),
        (taxa[2], taxa[5]),
    }


@then("the producers are as expected")
def _(exch_tuples, exch_taxa, taxa):
    """the producers are as expected."""
    assert exch_tuples.producers == {taxa[0], taxa[1], taxa[2]}
    assert exch_taxa.producers == {taxa[0], taxa[1], taxa[2]}


@then("the consumers are as expected")
def _(exch_tuples, exch_taxa, taxa):
    """the consumers are as expected."""
    assert exch_tuples.consumers == {taxa[3], taxa[4], taxa[5]}
    assert exch_taxa.consumers == {taxa[3], taxa[4], taxa[5]}


@then("the two exchanges are the same")
def _(exch_tuples, exch_taxa):
    """the two exchanges are the same."""
    assert exch_tuples == exch_taxa
