"""Answer class feature tests."""

import re
from pathlib import Path

import clyngor
from pytest_bdd import given, scenario, then, when

from cocomico.answer import Answer
from cocomico.base import Biomolecule, Exchange, Metabolite, Reaction, Taxon


@scenario("answer.feature", "decode answers")
def test_decode_answers():
    """decode answers."""


@given("a knowledge base", target_fixture="kb_facts")
def _():
    """a knowledge base."""
    seeds = Path("tests") / "data_test" / "kb" / "seeds.lp"
    facts = Path("tests") / "data_test" / "kb" / "Com1.lp"
    return [facts, seeds]


@given("score_program", target_fixture="lp_program")
def _():
    """score_program."""
    return Path("src") / "cocomico" / "encodings" / "general_scope.lp"


@when("solve and decode", target_fixture="answers")
def _(lp_program, kb_facts):
    """solve and decode."""
    *_, best_answer = clyngor.decode([lp_program, *kb_facts], decoders=[Answer])
    return best_answer


@then("answers have expected exchanges")
def _(answers):
    """answers have expected exchanges."""
    expected = {
        Biomolecule("M_A_c"): {Exchange(Taxon("Com1Org2"), Taxon("Com1Org3"))},
        Biomolecule("M_B_c"): {Exchange(Taxon("Com1Org3"), Taxon("Com1Org1"))},
        Biomolecule("M_C_c"): {
            Exchange(Taxon("Com1Org1"), Taxon("Com1Org2")),
            Exchange(Taxon("Com1Org1"), Taxon("Com1Org3")),
            Exchange(Taxon("Com1Org3"), Taxon("Com1Org2")),
        },
        Biomolecule("M_Y_c"): {Exchange(Taxon("Com1Org2"), Taxon("Com1Org3"))},
    }
    assert answers.exchange == expected


@then("answers have expected polyopsonies")
def _(answers):
    """answers have expected polyopsonies."""
    expected = {Biomolecule("M_C_c"): 2}
    assert answers.polyopsonist == expected


@then("answers have expected monopsonies")
def _(answers):
    """answers have expected monopsonies."""
    expected = {
        Biomolecule("M_A_c"): 1,
        Biomolecule("M_B_c"): 1,
        Biomolecule("M_Y_c"): 1,
    }
    assert answers.monopsonist == expected


@then("answers have expected polypolies")
def _(answers):
    """answers have expected polypolies."""
    expected = {Biomolecule("M_C_c"): 2}
    assert answers.polypolist == expected


@then("answers have expected monopolies")
def _(answers):
    """answers have expected monopolies."""
    expected = {
        Biomolecule("M_A_c"): 1,
        Biomolecule("M_B_c"): 1,
        Biomolecule("M_Y_c"): 1,
    }
    assert answers.monopolist == expected


@then("answers have expected scope")
def _(answers):
    """answers have expected scope."""
    assert "all" in answers.scope
    for m, t in [("M_C_c", "Com1Org3"), ("M_A_c", "Com1Org2"), ("M_M_c", "Com1Org2")]:
        assert (
            Metabolite(biomolecule=Biomolecule(m), provenance=Taxon(t))
            in answers.scope["all"]
        )


@then("answers have expected scope per community")
def _(answers):
    """answers have expected scope per community."""
    for taxon, biomolecules in [
        ("Com1Org1", {"M_C_c", "M_F2_c"}),
        ("Com1Org2", {"M_A_c", "M_F_c", "M_M_c", "M_O_c", "M_P_c", "M_Q_c", "M_S_c"}),
        ("Com1Org3", {"M_B_c", "M_V_c"}),
    ]:
        cname = f'self("{ taxon }")'
        assert cname in answers.scope
        individual_scope = {
            Metabolite(biomolecule=Biomolecule(m), provenance=Taxon(taxon))
            for m in biomolecules
        }
        assert individual_scope == answers.scope[cname]


@then("answers have expected activated per community")
def _(answers):
    """answers have expected activated per community."""

    assert answers.activated

    for cname, reactions in answers.activated.items():
        for reaction in reactions:
            assert cname in [f'self("{ reaction.taxon }")', "all"]

    added = set(
        (
            "R_A2_B2, R_A2_B2rev, R_B_JD, R_C2_B2, R_C2_B2rev, R_C_C2, R_C_C2rev, "
            "R_C_PY, R_D_X, R_D_Xrev, R_G_H, R_G_Hrev, R_H_C, R_I_H, R_I_Hrev, "
            "R_W_A2, R_W_A2rev, R_W_B, R_X_G, R_X_Grev, R_Y_W"
        ).split(", ")
    )

    expected = [
        ("Com1Org1", "R_F2_C, R_F2_Crev, R_X1_F2"),
        (
            "Com1Org2",
            "R_A_S, R_A_Srev, R_F_M, R_F_Mrev, R_M_O, R_M_Q, R_M_Qrev, R_O_P, "
            "R_O_Prev, R_Q_A, R_Q_Arev",
        ),
        ("Com1Org3", "R_E_V, R_V_B, R_V_Brev"),
    ]

    for community, reactions in expected:
        cname = f'self("{ community }")'
        assert cname in answers.activated
        individual = {Reaction(r, Taxon(community)) for r in reactions.split(", ")}
        assert individual == answers.activated[cname]
        assert not {r.name for r in individual} <= added


@then("string representation of answers is good")
def _(answers):
    """string representation of answers is good."""
    output = str(answers)
    for key in ["scope", "exch", "C>1", "C=1", "P>1", "P=1"]:
        assert key in output
    r_answers = r"<scope (.*?); exch (.*?); C>1 (.*?); C=1 (.*?); P>1 (.*?); P=1 (.*?)>"
    assert (g := re.search(r_answers, output)) is not None
    for x in g.group(1).split(" "):
        assert x in [str(b) for b in answers.scope["all"]]
    for x in g.group(3).split(" "):
        assert x in [str(b) for b in answers.polyopsonist]
    for x in g.group(4).split(" "):
        assert x in [str(b) for b in answers.monopsonist]
    for x in g.group(5).split(" "):
        assert x in [str(b) for b in answers.polypolist]
    for x in g.group(6).split(" "):
        assert x in [str(b) for b in answers.monopolist]
    for x in re.findall(r"(\w+):{[^}]*}", g.group(2)):
        assert x in answers.exchange
