cwlVersion: v1.2
class: Workflow

requirements:
  InlineJavascriptRequirement: {}
  LoadListingRequirement: { loadListing: shallow_listing }
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  name: string
  csv_output: string
  seeds_file: File[]
  sbml_dir: Directory
  specs:
    type:
      type: array
      items:
        - File
        - Directory

outputs:
  csv:
    type: File
    outputSource: concatenate/concat
  listing:
    type: File[]
    outputSource: get_file_list/listing
  each_csv:
    type: File[]?
    outputSource: cocomico/csv

label: cocomico_experiment

doc: |-
  Scatter CoCoMiCo over a collection of specifications and seeds
  and concatenate the results in a single CSV table.

steps:

  get_file_list:
    run:
      class: ExpressionTool
      label: "List specs"
      doc: |-
        Get a file list, either explicitly, or implicitly from a directory.
        It is convenient to specify whole directories since sample generation
        can split specs into multiple files in order to improve parallelism.
      inputs:
        files:
          type:
            type: array
            items:
              - File
              - Directory
        regex:
          type: string
          default: null
      outputs:
        listing: File[]
      expression: |
        ${
          const is_file = (f) => f.class == "File";
          const is_regex = (f) => regex.test(f.basename);
          const compare_name = (a,b) => a.nameroot.localeCompare(b.nameroot, undefined);
          const regex = inputs.regex ? new RegExp(inputs.regex) : /.*json$/;
          var listing = [];
          for (var i=0; i<inputs.files.length; i++) {
            listing = listing.concat(inputs.files[i].class == "Directory" ? inputs.files[i].listing.sort(compare_name) : inputs.files[i])
          }
          listing = listing.filter(is_file).filter(is_regex);
          return {"listing": listing};
        }
    in:
      files: specs
    out: [listing]

  cocomico:
    run: cocomico_spec.cwl
    scatter: [spec]
    scatterMethod: dotproduct
    in:
      spec: get_file_list/listing
      csv_output:
        valueFrom: "output.csv"
      header_csv:
        valueFrom: $(false)
      seeds_file: seeds_file
      sbml_dir: sbml_dir
    out: [csv]

  concatenate:
    run:
      class: CommandLineTool
      cwlVersion: v1.2
      baseCommand: ['cat', '/dev/null']
      inputs:
        files:
          type: File[]?
          inputBinding:
            position: 1
        csv_output: string
      outputs:
        concat:
          type: stdout
      stdout: $(inputs.csv_output)
    in:
      files:
        source: cocomico/csv
      csv_output:
        source: csv_output
    out: [concat]


s:author:
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005

s:codeRepository: https://gitlab.inria.fr/CCMC/CoCoMiCo.git
s:license: https://spdx.org/licenses/GPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2023-08-29"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.25.owl
