Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Analyze communities from a directory
    Given seed file
    And SBML models directory
    And output path
    When run cocomico
    Then have result files
    And community from directory has expected taxa
