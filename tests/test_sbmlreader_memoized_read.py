"""SBML reader feature tests."""

from pathlib import Path
from typing import List, Tuple

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Taxon
from cocomico.model import Model
from cocomico.sbmlreader import SbmlReader


@scenario("sbmlreader_memoized_read.feature", "Read models")
def test_read_models():
    """Read models."""


@given("an sbml reader", target_fixture="sbml_reader")
def _(sbml_dir: Path) -> SbmlReader:
    """an sbml reader."""
    return SbmlReader(sbml_dir)


@when(parsers.parse("read model from {file:S}"), target_fixture="taxon_model_path")
def _(file: Path, sbml_dir: Path, sbml_reader: SbmlReader) -> Tuple[Taxon, Model, Path]:
    """read model from <path>."""
    taxon, model = sbml_reader.from_file(sbml_dir / file)
    return taxon, model, sbml_dir / file


@then(parsers.parse("the file maps to expected {taxon:S}"))
def _(
    taxon: Taxon, sbml_reader: SbmlReader, taxon_model_path: Tuple[Taxon, Model, Path]
):
    """the file maps to expected <taxon>."""
    discovered_taxon, _, path = taxon_model_path
    assert sbml_reader.files[path] == discovered_taxon
    assert sbml_reader.files[path] == taxon


@then(parsers.parse("the model has the expected {reactions:S}"))
def _(taxon_model_path: Tuple[Taxon, Model, Path], reactions: str):
    """the model has the expected <reactions>."""
    _, model, _ = taxon_model_path
    assert {r.name for r in model.reactions} == set(reactions.split(","))


@scenario("sbmlreader_memoized_read.feature", "Read models several times")
def test_read_models_several_times():
    """Read models several times."""


@when("read the files several times")
def _(models: List[Path], sbml_reader: SbmlReader):
    """read the files several times."""
    for path in models:
        for _ in range(10):
            _ = sbml_reader.from_file(path)


@then("the reader records each file only once")
def _(models: List[Path], sbml_reader: SbmlReader):
    """the reader records each file only once."""
    assert sorted(sbml_reader.files.keys()) == sorted(models)


@then("the reader records each taxon only once")
def _(sbml_reader: SbmlReader):
    """the reader records each taxon only once."""
    assert len(sbml_reader.files.keys()) == len(sbml_reader.models.keys())
    assert set(sbml_reader.files.values()) == set(sbml_reader.models.keys())


@scenario("sbmlreader_memoized_read.feature", "Read model without sbml_dir")
def test_read_model_without_sbml_dir():
    """Read model without sbml_dir."""


@given("an sbml reader without sbml_dir", target_fixture="sbml_reader_wo")
def _() -> SbmlReader:
    """an sbml reader without sbml_dir."""
    return SbmlReader()


@when("read a set of models")
def _(sbml_reader_wo: SbmlReader, sbml_dir: Path):
    """read a set of models."""
    for path in [
        "Com1Org1.sbml",
        "Com1Org1.sbml",
        "Com1Org2.sbml",
        "../sbml/Com1Org1.sbml",
        "../sbml_nonstd/Com1Org1_alias.sbml",
    ]:
        taxon, model = sbml_reader_wo.from_file(sbml_dir / path)
        assert taxon
        assert model


@then("the set of taxa are correct")
def _(sbml_reader_wo: SbmlReader):
    """the set of taxa are correct."""
    assert set(sbml_reader_wo.models.keys()) == {Taxon("Com1Org1"), Taxon("Com1Org2")}
    assert set(sbml_reader_wo.files.values()) == set(sbml_reader_wo.models.keys())


@scenario("sbmlreader_memoized_read.feature", "Read compressed model")
def test_read_compressed_model():
    """Read compressed model."""


@when("read compressed models")
def _(sbml_reader_wo: SbmlReader, sbml_dir: Path):
    """read compressed models."""
    for path in [
        "Com1Org1.sbml",
        "Com1Org2.sbml.gz",
        "Com1Org2.sbml",
        "Com1Org2",
    ]:
        taxon, model = sbml_reader_wo.from_file(
            Path("tests/data_test/compressed") / path
        )
        assert taxon
        assert model
