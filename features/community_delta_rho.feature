Feature: Community
    Characterize added value of scope and of reaction activations

    Scenario Outline: Compute metrics delta rho
        Given community of <organisms>
        And SBML models directory
        And seeds
        When build community
        Then each community has the expected <delta> <cscope> <iscope> <rho> <cactiv> <iactiv>

        Examples:
        | organisms                                    | delta  | cscope | iscope | rho   | cactiv | iactiv |
        | Com1Org1,Com1Org2,Com1Org3                   | 12     | 23     | 11     | 21    | 38     | 17     |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4          | 19     | 33     | 14     | 35    | 55     | 20     |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | 20     | 41     | 21     | 37    | 68     | 31     |
        | Com1Org1,Com2Org4                            |  5     | 10     |  5     | 10    | 16     |  6     |
        | Com3Org1,Com3Org2,Com3Org3,Com3Org4          | 19     | 33     | 14     | 35    | 55     | 20     |
        | Com3Org1,Com3Org2,Com3Org3                   | 12     | 23     | 11     | 21    | 38     | 17     |
        | Com3Org1,Com3Org2,Com3Org4                   | 13     | 25     | 12     | 23    | 40     | 17     |
