"""Report feature tests."""

import json

from pytest import approx
from pytest_bdd import given, scenario, then, when

from cocomico import report, score
from cocomico.base import ExchangeNM, Seeds


@scenario("report_json.feature", "JSON report")
def test_json_report():
    """JSON report."""


@given("JSON output path", target_fixture="output_path")
def _(tmp_path):
    """JSON output path."""
    return tmp_path / "community.json"


@when("write JSON report")
def _(community, output_path):
    """create JSON report."""
    report.write_json(
        communities={str(output_path.with_suffix("").name): community},
        output_dir=output_path.parent,
    )


@when("reread JSON report", target_fixture="from_json")
def _(output_path):
    """reread JSON report."""
    with open(output_path, mode="r", encoding="UTF-8") as json_file:
        return json.load(json_file)


@then("community and JSON report agree")
def _(community, from_json):
    """community and JSON report agree."""
    assert set(from_json["taxa"]) == community.taxa
    assert set(from_json["models"]) == set(str(p) for p in community.files.values())
    assert int(from_json["size"]) == len(community.taxa)

    for result in from_json["results"]:
        seeds = Seeds(result["seeds"])

        assert result["polyopsonist"] == community.polyopsonist(seeds)
        assert result["monopsonist"] == community.monopsonist(seeds)
        assert set(result["scope"]) == set(str(m) for m in community.scope(seeds))

        assert set(result["activated"]) == set(
            str(m) for m in community.activated(seeds)
        )
        assert set(result["produced_seeds"]) == community.produced_seeds(seeds)
        assert set(result["consumed_seeds"]) == community.consumed_seeds(seeds)

        assert result["competition"] == list(
            approx(x) for x in score.competition(community, seeds)
        )
        assert result["cooperation"] == list(
            approx(x) for x in score.cooperation(community, seeds)
        )
        assert result["delta"] == list(approx(x) for x in score.delta(community, seeds))
        assert result["rho"] == list(approx(x) for x in score.rho(community, seeds))

        # Quick check that exchange results from community must be found.
        # Contrast with complete dict equality checked in test_report_serialize.py.
        for k in community.exchange(seeds).keys():
            res_nm = ExchangeNM(
                producers=result["exchange"][k]["producers"],
                consumers=result["exchange"][k]["consumers"],
            )
            com_nm = ExchangeNM(relations=community.exchange(seeds)[k])
            assert res_nm == com_nm


@then("JSON report contains expected keys")
def _(from_json):
    """JSON report contains expected keys."""
    expected = set(["taxa", "models", "size"])
    assert expected <= set(from_json.keys())
