"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import given, scenario, then, when

from cocomico import score
from cocomico.base import Biomolecule, MetaboliteSet, Seeds
from cocomico.community import Community


@scenario("community_no_growth.feature", "Calculate activated")
def test_calculate_activated():
    """Calculate activated."""


@given("community of organisms", target_fixture="community")
def _():
    """community of organisms."""
    sbml_dir = Path("tests") / "data_test" / "sbml"
    community = Community(sbml_dir=sbml_dir)
    return community


@given("useless seeds", target_fixture="seeds")
def _():
    """useless seeds."""
    return Seeds([Biomolecule(m) for m in ["M_Un1_c", "M_Un2_c"]])


@when("we compute answers")
def _():
    """we compute answers."""


@then("scope wrt seeds is empty")
def _(community, seeds):
    """scope wrt seeds is empty."""
    assert community.scope(seeds) == MetaboliteSet({})


@then("activated wrt seeds is empty")
def _(community, seeds):
    """activated wrt seeds is empty."""
    assert community.activated(seeds) == set()


@then("consumed_seeds wrt seeds is empty")
def _(community, seeds):
    """consumed_seeds wrt seeds is empty."""
    assert community.consumed_seeds(seeds) == set()


@then("produced_seeds wrt seeds is empty")
def _(community, seeds):
    """produced_seeds wrt seeds is empty."""
    assert community.produced_seeds(seeds) == set()


@then("metrics wrt seeds are zero")
def _(community, seeds):
    """metrics wrt seeds are zero."""
    assert score.cooperation(community, seeds)[0] == 0
    assert score.competition(community, seeds)[0] == 0
    assert score.delta(community, seeds)[0] == 0
    assert score.rho(community, seeds)[0] == 0
