Feature: Community class
  Represent a microbial community

  Scenario Outline: Ignore seed reactions
    Given SBML noexchange directory
    When create community object from noexch directory
    And remove seed reactions
    Then all reactions have reactants and products
