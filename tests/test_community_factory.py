"""Community class feature tests."""

import json
from pathlib import Path

from pytest_bdd import parsers, scenario, then, when

from cocomico.community import Community


@scenario("community_factory.feature", "Initialize a set of communities")
def test_initialize_a_set_of_communities():
    """Initialize a set of communities."""


@when("create community objects", target_fixture="communities")
def _(community_json: Path, sbml_dir: Path) -> dict[str, Community]:
    """create community objects."""
    return Community.from_specification(spec=community_json, sbml_dir=sbml_dir)


@then(parsers.parse("each {community:S} has expected {taxa:S}"))
def _(communities, community, taxa):
    """each <community> has expected <taxa>."""
    assert communities[community].taxa == set(taxa.split(","))


@then("community files match the definition")
def _(communities, community_json):
    """community files match the definition."""
    with open(community_json, mode="r", encoding="UTF-8") as json_file:
        definition = json.load(json_file)
    for name, community in communities.items():
        file_names = {str(p.name) for p in community.files.values()}
        # File names in a community definition file are relative to sbml_dir
        # unless absolute, so resolve the path then relativize to sbml_dir.
        # Will throw an exception if the test data specifies a nonabsolute
        # path that isn't under sbml_dir.
        def_names = {
            str((community.sbml_dir / p).relative_to(community.sbml_dir).name)
            for p in definition[name]
        }
        # Check that the file names in the community match the definition.
        assert file_names == def_names
