Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Check output options
    Given seed file
    And SBML models directory
    And model files
    And with_json output path
    And with_csv output path
    And without_json output path
    And without_csv output path
    When run cocomico with json 
    And run cocomico with csv
    And run cocomico without json 
    And run cocomico without csv
    Then have expected result files
