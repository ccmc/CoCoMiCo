"""CWL entrypoint feature tests."""

import json
from pathlib import Path

from pytest_bdd import given, parsers, scenario, then, when


@scenario("script_cocomico_cwl_spec.feature", "Analyze communities from specification")
def test_analyze_one_community():
    """Analyze communities from specification."""


@given("a community specification file", target_fixture="spec")
def _() -> Path:
    """a community specification file."""
    return Path("tests") / "data_test" / "communities.json"


@given("output directory path", target_fixture="output")
def _(tmp_path) -> Path:
    """output directory path."""
    (output := tmp_path / "results").mkdir(parents=True, exist_ok=True)
    return output


@when("run cocomico")
def _(seed_file, sbml_dir, spec, output, helpers):
    """run cocomico."""

    result = helpers.invoke(
        "specification",
        seed=seed_file,
        sbml=sbml_dir,
        output=output,
        extra=["--no-header-csv", "--spec", str(spec)],
    )
    assert result.exit_code == 0


@then("have result files")
def _(output):
    """have result files."""
    assert output.is_dir()


@then(parsers.parse("each {community:S} has expected {taxa:S}"))
def _(community, taxa, output):
    """each <community> has expected <taxa>."""
    community_file = output / f"{ community }.json"
    assert community_file.is_file()

    with open(community_file, mode="r", encoding="UTF-8") as json_file:
        from_json = json.load(json_file)
        assert "taxa" in from_json
        for taxon in taxa.split(","):
            assert taxon in from_json["taxa"]
