"""cocomico.utils feature tests."""

from pathlib import Path

from clyngor.as_pyasp import Atom
from pytest_bdd import given, scenario, then, when

from cocomico.base import Biomolecule, Metabolite
from cocomico.community import Community
from cocomico.facts import network_facts


@scenario("facts_network.feature", "network facts")
def test_facts_network():
    """network facts."""


@given("an SBML file", target_fixture="sbml_file")
def _():
    """an SBML file."""
    return Path("tests") / "data_test" / "sbml" / "Com1Org1.sbml"


@when("create community object", target_fixture="community")
def _(sbml_file):
    """create community object."""
    community = Community(models=[Path(sbml_file.name)], sbml_dir=sbml_file.parent)
    assert len(community.keys()) > 0
    return community


@when("generate facts for the community", target_fixture="facts_v3")
def _(community):
    """generate facts for the community."""
    facts_biomolecules_false = network_facts(community.models, biomolecules_only=False)
    assert facts_biomolecules_false
    return facts_biomolecules_false


@when("generate v2 facts for the community", target_fixture="facts_v2")
def _(community):
    """generate v2 facts for the community."""
    facts_biomolecules_true = network_facts(community.models, biomolecules_only=True)
    assert facts_biomolecules_true
    return facts_biomolecules_true


@then("returned facts are as expected")
def _(facts_v2, facts_v3):
    """returned facts are as expected."""

    # determined by choice of SBML file
    taxon = "Com1Org1"

    # Facts that are the same in v2 and v3.
    # When testing Atoms, include " that Clyngor wraps them in

    for facts in [facts_v2, facts_v3]:
        # number of facts
        for predicate, count in {
            "taxon": 1,
            "reaction": 14,
            "biomolecule": 11,
        }.items():
            assert count == len([a for a in facts if a.predicate == predicate])

        # taxon facts
        assert Atom("taxon", [f'"{ taxon }"']) in facts

        # reaction facts
        reactions = {a for a in facts if a.predicate == "reaction"}
        assert len(reactions) == 14
        rids = {r.arguments[0] for r in reactions}
        revs = {id for id in rids if id.endswith('rev"')}
        assert revs == {
            f'"R_{ p }_{ c }rev"'
            for p, c in [("X", "G"), ("I", "H"), ("F2", "C"), ("D", "X"), ("G", "H")]
        }

        # metabolite facts
        metabolite_biomolecules = {a for a in facts if a.predicate == "biomolecule"}
        assert metabolite_biomolecules == {
            Atom("biomolecule", [f'"M_{ short }_c"', f'"{ taxon }"'])
            for short in ["J", "B", "C", "D", "X", "G", "H", "I", "K", "F2", "X1"]
        }

    # Facts for product, reactant are different in v2 and v3.

    correct_biomolecules = {
        kind: {Biomolecule(f"M_{ short }_c") for short in short_biomolecules}
        for kind, short_biomolecules in {
            "product": ["J", "C", "D", "X", "G", "H", "I", "F2"],
            "reactant": ["B", "C", "D", "X", "G", "H", "I", "K", "F2", "X1"],
        }.items()
    }

    for kind in ["product", "reactant"]:
        for facts in [facts_v2, facts_v3]:
            metabolites = {
                # Our metabolites don't want the " that clyngor wraps atoms in.
                Metabolite.from_string(a.arguments[0].strip('"'))
                for a in facts
                if a.predicate == kind
            }
            assert {m.biomolecule for m in metabolites} == correct_biomolecules[kind]

            if facts is facts_v3:
                for m in metabolites:
                    assert m.provenance == taxon
