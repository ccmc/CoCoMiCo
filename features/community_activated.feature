Feature: Community class
  Represent a microbial community

  Scenario Outline: Calculate activated
    Given community of <organisms>
    And SBML models directory
    And seeds
    When build community
    And choose <community_choice>
    And compute activated wrt seeds for community choice
    Then activated wrt seeds for choice is <expected_activated>

    Examples:
    | organisms | community_choice | expected_activated |
    | Com1Org2,Com1Org3 | None | R_A_S,R_A_Srev,R_E_V,R_F_M,R_F_Mrev,R_M_O,R_M_Q,R_M_Qrev,R_O_P,R_O_Prev,R_Q_A,R_Q_Arev,R_V_B,R_V_Brev |
    | Com1Org1,Com1Org2,Com1Org3 | Com1Org1 | R_F2_C,R_F2_Crev,R_X1_F2 |
    | Com1Org1,Com1Org2,Com1Org3 | Com1Org2 | R_A_S,R_A_Srev,R_F_M,R_F_Mrev,R_M_O,R_M_Q,R_M_Qrev,R_O_P,R_O_Prev,R_Q_A,R_Q_Arev |
    | Com1Org1,Com1Org2,Com1Org3 | Com1Org3 | R_E_V,R_V_B,R_V_Brev |
    | Com3Org1,Com3Org2,Com3Org3,Com3Org4 | Com3Org3 | R_E_V,R_V_B,R_V_Brev |
