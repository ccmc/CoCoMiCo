"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import scenario, then, when

from cocomico.community import Community


@scenario("community_dir.feature", "Initialize a community from a directory")
def test_initialize_a_community():
    """Initialize a community from a directory."""


@when("create community object from directory", target_fixture="community")
def _(sbml_dir: Path) -> Community:
    """create community object from directory."""
    community = Community(sbml_dir=sbml_dir)
    files = [str(f.stem) for f in sbml_dir.iterdir()]
    assert set(community.keys()) == set(files)
    return community


@then("the community has expected models")
def _(community: Community):
    """the community has expected models."""
    assert set(community.keys()) == set(
        [
            "Com1Org1",
            "Com1Org2",
            "Com1Org3",
            "Com2Org1",
            "Com2Org2",
            "Com2Org3",
            "Com2Org4",
            "Com3Org1",
            "Com3Org2",
            "Com3Org3",
            "Com3Org4",
        ]
    )
    assert set(community.keys()) == set(community.sbml_reader.files.values())
