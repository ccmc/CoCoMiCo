Feature: Facts
    LP facts generated from relations in models

    Scenario Outline: network facts
    Given an SBML file
	When create community object
	And generate facts for the community
	And generate v2 facts for the community
	Then returned facts are as expected
