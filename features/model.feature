Feature: Model class
  Relations defining a GSMN

  Scenario Outline: Initialize a model
    Given a taxon
    And a set of metabolites
    And a set of reactions
    And a set of metabolite-reaction relations
    When create model object
    Then model biomolecule are as expected
    And model reactions are as expected
    And model relations are as expected

  Scenario Outline: Initialize a model ignoring seed reactions
    Given a taxon
    And a set of metabolites
    And a set of reactions
    And a set of metabolite-reaction relations
    When create model object ignoring seed reactions
    Then model biomolecule are as expected
    And model reactions are as expected without seed reactions
    And model relations are as expected without seed reactions
