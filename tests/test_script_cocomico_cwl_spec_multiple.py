"""CWL entrypoint feature tests."""

from pathlib import Path
from typing import List

from pytest_bdd import given, parsers, scenario, then, when


@scenario(
    "script_cocomico_cwl_spec_multiple.feature", "Analyze multiple specs from args"
)
def test_analyze_multiple_specs_from_args():
    """Analyze multiple specs from args."""


@scenario(
    "script_cocomico_cwl_spec_multiple.feature", "Analyze multiple specs from directory"
)
def test_analyze_multiple_specs_from_directory():
    """Analyze multiple specs from directory."""


@scenario(
    "script_cocomico_cwl_spec_multiple.feature", "Analyze multiple specs from options"
)
def test_analyze_multiple_specs_from_options():
    """Analyze multiple specs from options."""


@given("output directory path", target_fixture="output")
def _(tmp_path) -> Path:
    """output directory path."""
    (output := tmp_path / "results").mkdir(parents=True, exist_ok=True)
    return output


@given("directory with spec files", target_fixture="specs_d")
def _() -> Path:
    """output directory path."""
    return Path("tests") / "data_test" / "specs_d"


@given(parsers.parse("multiple specs {spec_files:S}"), target_fixture="specs")
def _(spec_files, specs_d: Path) -> List[Path]:
    """multiple specs <spec_files>."""
    specs = [specs_d / f for f in spec_files.split(",")]
    for f in specs:
        assert f.is_file()
    return specs


@when("run cocomico spec options")
def _(seed_file, sbml_dir, specs, output, helpers):
    """run cocomico spec options."""

    result = helpers.invoke(
        "specification",
        seed=seed_file,
        sbml=sbml_dir,
        output=output,
        extra=["--no-header-csv"] + [i for sp in specs for i in ["--spec", sp]],
    )
    assert result.exit_code == 0


@when("run cocomico spec args")
def _(seed_file, sbml_dir, specs, output, helpers):
    """run cocomico spec args."""

    result = helpers.invoke(
        "specification",
        seed=seed_file,
        sbml=sbml_dir,
        output=output,
        # extra=["--no-header-csv"] + [str(sp) for sp in specs],
        extra=["--no-header-csv", *specs],
    )
    assert result.exit_code == 0


@when("run cocomico spec directory")
def _(seed_file, sbml_dir, specs_d, output, helpers):
    """run cocomico spec args."""

    result = helpers.invoke(
        "specification",
        seed=seed_file,
        sbml=sbml_dir,
        output=output,
        extra=["--no-header-csv", specs_d],
    )
    assert result.exit_code == 0


@then("have result files")
def _(output):
    """have result files."""
    assert output.is_dir()


@then(parsers.parse("have expected {communities:S}"))
def _(communities, output):
    """have expected <communities>."""
    for community in communities.split(","):
        community_file = output / f"{ community }.json"
        assert community_file.is_file()
