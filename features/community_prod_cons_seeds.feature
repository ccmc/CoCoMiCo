Feature: Community class
    Represent a microbial community

    Scenario Outline: Calculate produced and consumed seeds
        Given community of <organisms>
        And SBML models directory
        And seeds
        When build community
        And choose <community_choice>
        Then <produced> and <consumed> seeds are as expected

        Examples:
        | organisms                  | community_choice | produced | consumed           |
        | Com3Org2,Com3Org3          | None             | M_F_c    | M_E_c,M_F_c        |
        | Com3Org1,Com3Org2,Com3Org3 | Com3Org2         | M_F_c    | M_F_c              |
        | Com3Org1,Com3Org2,Com3Org3,Com3Org4 | None    | M_F_c    | M_E_c,M_F_c,M_X1_c |
