Feature: Answer class
  Represent answers from clingo

  Scenario Outline: decode answers
    Given score_program
    And a knowledge base
    When solve and decode
    Then answers have expected scope
    And answers have expected exchanges
    And answers have expected polyopsonies
    And answers have expected monopsonies
    And answers have expected polypolies
    And answers have expected monopolies
    And answers have expected activated per community
    And answers have expected scope per community
    And string representation of answers is good
