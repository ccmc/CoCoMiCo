Feature: Community class
  Represent a microbial community

  Scenario Outline: Models can be shared between communities
    Given SBML models directory
    And model files
    When create communities with reused models
    Then communities are as expected
    And reused models are shared
