"""Community class feature tests."""

from typing import Union

from pytest_bdd import parsers, scenario, then, when

from cocomico.base import Biomolecule, Seeds, Taxon
from cocomico.community import Community


@scenario("community_prod_cons_seeds.feature", "Calculate produced and consumed seeds")
def test_calculate_produced_and_consumed_seeds():
    """Calculate produced and consumed seeds."""


@when(parsers.parse("choose {community_choice:S}"), target_fixture="choice")
def _(community_choice) -> Union[Taxon, None]:
    """choose <community_choice>."""
    # Sentinal value because pytest-bdd has troubel parsing empty example value
    if community_choice is None or community_choice in ["all", "None"]:
        return None
    return Taxon(community_choice)


@then(parsers.parse("{produced:S} and {consumed:S} seeds are as expected"))
def _(
    produced: str,
    consumed: str,
    seeds: Seeds,
    community: Community,
    choice: Union[Taxon, None],
):
    """<produced> and <consumed> seeds are as expected."""
    expected_produced = Seeds({Biomolecule(m) for m in produced.split(",")})
    observed_produced = community.produced_seeds(seeds, choice=choice)
    assert observed_produced == expected_produced

    expected_consumed = Seeds({Biomolecule(m) for m in consumed.split(",")})
    observed_consumed = community.consumed_seeds(seeds, choice=choice)
    assert observed_consumed == expected_consumed
