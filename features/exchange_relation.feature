Feature: Exchange class
  Represent an exchange between taxa

  Scenario Outline: Create exchanges and iterate over them
    Given a collection of taxa
    And a collection of input tuples
    When create an exchange from tuples
    And create an exchange from taxa
    Then input tuples are in the exchanges
    And the two exchanges are the same
    And the producers are as expected
    And the consumers are as expected
    And iterating produces the expected tuples
