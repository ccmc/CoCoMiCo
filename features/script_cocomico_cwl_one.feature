Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Analyze one community
    Given seed file
    And SBML models directory
    And model files
    And json output path
    And tabular output path
    When run cocomico
    Then have result files
    And JSON file has results
    And tabular file has results
