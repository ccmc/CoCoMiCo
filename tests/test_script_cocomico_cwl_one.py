"""CWL entrypoint feature tests."""

import json

from pytest import approx
from pytest_bdd import given, scenario, then, when

from cocomico import report


@scenario("script_cocomico_cwl_one.feature", "Analyze one community")
def test_analyze_one_community():
    """Analyze one community."""


@given("json output path", target_fixture="output_json")
def _(tmp_path):
    """json output path."""
    return tmp_path / "results.json"


@given("tabular output path", target_fixture="output_csv")
def _(tmp_path):
    """tabular output path."""
    return tmp_path / "results.csv"


@when("run cocomico")
def _(seed_file, sbml_dir, models, output_json, output_csv, helpers):
    """run cocomico."""
    # pylint: disable=R0913
    result = helpers.invoke(
        "community",
        seed=seed_file,
        sbml=sbml_dir,
        json=output_json,
        csv=output_csv,
        models=models,
        extra=["--no-header-csv"],
    )
    assert result.exit_code == 0


@then("have result files")
def _(output_json, output_csv):
    """have result files."""
    assert output_json.is_file()
    assert output_csv.is_file()


@then("JSON file has results")
def _(output_json, models):
    """JSON files has results."""
    with open(output_json, mode="r", encoding="UTF-8") as json_file:
        from_json = json.load(json_file)
        assert "taxa" in from_json
        assert "results" in from_json
        for model in models:
            assert str(model.name) in from_json["models"]
        for result in from_json["results"]:
            assert "activated" in result
            assert "exchange" in result


@then("tabular file has results")
def _(output_csv):
    """tabular file has results."""
    table = report.read_tabular(output_csv)
    for name, row in table.items():
        assert name == "erehwon"
        assert len(row) >= 9
        assert len(row[0]) > 0
        assert approx(float(row[1]), 0.1) == 20  # delta
        assert approx(float(row[2]), 0.1) == 19.25  # coop
        assert approx(float(row[3]), 0.1) == 37  # rho
        assert approx(float(row[4]), 0.1) == 1.8  # comp
