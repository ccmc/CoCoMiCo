"""Metabolite class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, Taxon


@scenario("metabolite_from_string.feature", "Initialize a metabolite from string")
def test_initialize_a_metabolite_from_string():
    """Initialize a metabolite from string."""


@given(parsers.parse("a {metabolite_id:S} string"), target_fixture="id_string")
def _(metabolite_id: str):
    """a <metabolite_id> string."""
    return metabolite_id


@when("create metabolite", target_fixture="metabolite")
def _(id_string: str):
    """create metabolite."""
    metabolite = Metabolite.from_string(id_string)
    assert id_string in str(metabolite)
    return metabolite


@then(parsers.parse("metabolite has expected {provenance:S} and {biomolecule:S}"))
def _(metabolite: Metabolite, provenance: Taxon, biomolecule: Biomolecule):
    """metabolite has expected <provenance> and <biomolecule>."""
    assert metabolite.provenance == "" if provenance == "_none_" else provenance
    assert metabolite.biomolecule == biomolecule
