Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Ignore seed reactions
    Given SBML noexchange directory
    And empty seeds
    And tabular output path
    When run cocomico ignoring seed reactions
    Then tabular file has no exchanges
