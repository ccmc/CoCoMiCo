Feature: Community class
    Represent a microbial community

    Scenario Outline: Calculate scope by community
        Given community of <organisms>
        And SBML models directory
        And seeds
        When build community
        And choose <individual>
        And compute scope wrt seeds for individual
        Then scope wrt seeds for individual is <expected_scope>

        Examples:
        | organisms                  | individual | expected_scope |
        | Com1Org1,Com1Org2,Com1Org3 | Com1Org1   | M_C_c,M_F2_c |
        | Com1Org1,Com1Org2,Com1Org3 | Com1Org2   | M_A_c,M_F_c,M_M_c,M_O_c,M_P_c,M_Q_c,M_S_c |
        | Com1Org1,Com1Org2,Com1Org3 | Com1Org3   | M_B_c,M_V_c |
        | Com3Org1,Com3Org2,Com3Org3,Com3Org4 | Com3Org3 | M_B_c,M_V_c |
