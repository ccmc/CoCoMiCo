Feature: Sampling of communities
  Generate communities by sampling communities and ecosystems

  Scenario Outline: Write sample
    Given an ecosystem dict
    And output directory
    When write specs to a directory
    Then the spec equals the sample

  Scenario Outline: Write sample in parts
    Given an ecosystem dict
    And output directory
    When write specs in parts to a directory
    Then individual specs are parts of the sample
    And combined specs equal the complete sample
