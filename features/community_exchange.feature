Feature: Community class
Represent a microbial community

  Scenario Outline: Identify exchanged metabolites
    Given a community definition file
    And SBML models directory
    And a choice of <community>
    And seeds
    And a <metabolite> biomolecule
    And community objects
    When compute exchanged
    Then have expected <pairs> of exchanged (producer, consumer) taxa

    Examples:
    | community | metabolite | pairs                               |
    | com_0     | M_Y_c      | Com1Org2-Com1Org3                   |
    | com_0     | M_C_c      | Com1Org1-Com1Org2,Com1Org1-Com1Org3 |
    | com_1     | M_C_c      | Com2Org1-Com2Org2,Com2Org1-Com2Org3 |
    | com_1     | M_K_c      | Com2Org4-Com2Org1                   |
    | com_1     | M_Y_c      | Com2Org2-Com2Org3                   |
    | com_1     | M_P_c      | Com2Org2-Com2Org4                   |
    | com_2     | M_K_c      | Com2Org4-Com1Org1                   |
