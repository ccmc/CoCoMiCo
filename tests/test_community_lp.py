"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import scenario, then, when

from cocomico.base import Exchange, ExchangeNM, Metabolite, Seeds
from cocomico.community import Community


@scenario("community_lp.feature", "Get LP answers")
def test_get_lp_answers():
    """Get LP answers."""


@when("create community object", target_fixture="community")
def _(sbml_dir: Path) -> Community:
    """create community object."""
    models = [
        Path(f"Com{ i }Org{ j }.sbml")
        for i, j in [(3, 1), (3, 2), (3, 3), (3, 4), (1, 2)]
    ]
    community = Community(sbml_dir=sbml_dir, models=models)
    return community


@then("lp_fact contain expected keys")
def _(community: Community):
    """lp_fact contain expected keys."""
    knowledge_base = community.knowledge_base
    predicates = {a.predicate for a in knowledge_base}
    for key in [
        "taxon",
        "biomolecule",
        "reaction",
        "reactant",
        "product",
    ]:
        assert key in predicates


@then("answers for given seeds contain expected scope")
def _(community: Community, seeds: Seeds):
    """answers for given seeds contain expected scope."""
    answers = community.answers(seeds)
    assert answers.scope["all"]
    for scope in answers.scope.values():
        for answer in scope:
            assert isinstance(answer, Metabolite)


@then("answers for given seeds contain expected exchanges")
def _(community: Community, seeds: Seeds):
    """answers for given seeds contain expected scope."""
    answers = community.answers(seeds)
    assert answers.exchange
    for biomolecule, exchanges in answers.exchange.items():
        for e in exchanges:
            assert isinstance(e, Exchange)
        for taxon in [p[i] for i in [0, 1] for p in exchanges]:
            assert taxon in community.taxa
        assert biomolecule


@then("exchanges are cartesian product")
def _(community: Community, seeds: Seeds):
    """exchanges are cartesian product."""
    answers = community.answers(seeds)
    for relations in answers.exchange.values():
        assert (nm := ExchangeNM(relations=relations))
        for pair in [e for e in nm if e[0] != e[1]]:
            assert Exchange(*pair) in relations
