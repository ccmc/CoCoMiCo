Feature: Sampling of communities
  Generate communities by sampling communities and ecosystems

  @reset-name
  Scenario Outline: Name serial is initially empty
    Then name serial is initially empty

  Scenario Outline: Sample name from ecosystem
    Given an ecosystem <eco> <models> of <size>
    When make 1 sample name with eco and model list
    And make 5 sample names with eco, model list, and reps
    And make 2 sample names with eco, model list, and subs
    Then name has expected <string>
    And name has expected size, sub, rep
    And name eco is same as eco if given
    And name ident is <ident> otherwise
    And names have expected size and all reps
    And names have expected size and all subs

    Examples:
    | eco | models                     | size | ident | string |
    | eco | Com1Org1                   | 1    | TKI4HT6VPPBGBYOI7T57QVJVXR5UVW4L | eco_0_original_1_0  |
    | eco | Com1Org1,Com1Org2,Com1Org3 | 3    | FNCYJALXG5R75Q2SC7M7SP47U45U6A75 | eco_6_original_3_0  |
    | eco | Com2Org1,Com2Org3          | 2    | C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I | eco_12_original_2_0 |
    | None| Com2Org1,Com2Org3          | 2    | C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I | X_C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I_original_2_0 |
    | eco | None                       | 0    | 3I42H3S6NNFQ2MSVX7XZKYAYSCX5QBYJ | eco_18_original_0_0 |
    | None| None                       | 0    | 3I42H3S6NNFQ2MSVX7XZKYAYSCX5QBYJ | X_3I42H3S6NNFQ2MSVX7XZKYAYSCX5QBYJ_original_0_0 |

  Scenario Outline: Generate names from files
    Given SBML models directory
    When create community object from <models>
    And generate id
    And generate name
    Then generated ident is <ident>
    And generated name is (ident, original, None)
    And string name is ident_original_<size>_0
    And generated name with rep is (ident, original, rep)

    Examples:
    | models                     | size | ident |
    | Com1Org1                   | 1    | TKI4HT6VPPBGBYOI7T57QVJVXR5UVW4L |
    | Com1Org1,Com1Org2,Com1Org3 | 3    | FNCYJALXG5R75Q2SC7M7SP47U45U6A75 |
    | Com1Org2,Com1Org1,Com1Org3 | 3    | FNCYJALXG5R75Q2SC7M7SP47U45U6A75 |
    | Com2Org1,Com2Org3          | 2    | C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I |
    | None                       | 0    | 3I42H3S6NNFQ2MSVX7XZKYAYSCX5QBYJ |

  Scenario Outline: Sample names can be used for dicts
    Given an ecosystem <eco> <models> of <size>
    When make 5 sample names with eco, model list, and reps
    And make 2 sample names with eco, model list, and subs
    And make dicts using names
    Then dicts have 5 keys

    Examples:
    | eco | models                     | size | ident | string |
    | eco | Com2Org1,Com2Org3          | 2    | C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I | eco_12_original_2_0 |
    | None| Com2Org1,Com2Org3          | 2    | C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I | X_C5GZHD5IKUEG7LQHBJQIFIDFB66L3C7I_original_2_0 |

  @reset-name
  Scenario Outline: Sample names from names
    Given a name made from an <existing> string
    When make new name from existing and <eco> <sub> <rep> <size> <models>
    Then new name has requested attributes
    And new name has string <expected>

    Examples:
    | eco  | sub  | rep  | size | models | existing         | expected         |
    | None | None | None | None | None   | foo_0_original_5_1 | foo_0_original_5_1 |
    | bar  | None | None | None | None   | foo_0_original_5_1 | bar_0_original_5_1 |
    | bar  | qux  | None | None | None   | foo_0_original_5_1 | bar_0_qux_5_1      |
    | bar  | qux  | 2    | None | None   | foo_0_original_5_1 | bar_0_qux_5_2      |
    | bar  | qux  | 2    | 4    | None   | foo_0_original_5_1 | bar_0_qux_4_2      |
    | bar  | qux  | 2    | 4    | 1,2,3  | foo_0_original_5_1 | bar_0_qux_4_2      |
    | bar  | qux  | 2    | None | 1,2,3  | foo_0_original_5_1 | bar_0_qux_3_2      |
    | bar  | qux  | 2    | None | 1,2,3  | foo_0_original_0_1 | bar_0_qux_3_2      |
