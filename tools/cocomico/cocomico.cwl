class: CommandLineTool
cwlVersion: v1.2
label: cocomico
doc: "CoCoMiCo: COoperation and COmpetition in MIcrobial COmmunities"
hints:
  - class: DockerRequirement
    dockerPull: quay.io/biocontainers/python:3.10.4
requirements:
  - class: InlineJavascriptRequirement
#   - class: ScatterFeatureRequirement
inputs:
    seeds_file:
      type: File[]?
      doc: |
        SBML file defining seed biomolecules
      inputBinding:
        prefix: "--seeds-file"
    seeds_def:
      type: File[]?
      doc: |
        Comma-separated list of seed biomolecules
      inputBinding:
        prefix: "--seeds-def"
    sbml_dir:
      type: Directory?
      doc: |
        directory containing SBML model files
      inputBinding:
        prefix: "--sbml-dir"
    json_output:
      type: string?
      doc: |
        path to write JSON output
      inputBinding:
        prefix: "--json-output"
    csv_output:
      type: string?
      doc: |
        path to write CSV output
      inputBinding:
        prefix: "--csv-output"
    header_csv:
      type: boolean
      doc: |
        add header to CSV output
      default: False
      inputBinding:
        prefix: "--header-csv"
    name:
      type: string?
      doc: |
        community name
      default: "community"
      inputBinding:
        prefix: "--name"
    model:
      type: File[]?
      doc: |
        SBML models
      inputBinding:
        position: 1

    # Global options that must appear before the subcommand argument,
    # so they have negative positions.
    loglevel:
      type:
        type: enum
        symbols: ["DEBUG", "INFO", "WARN"]
      doc: |
        specify logging level INFO, DEBUG, WARN
      default: INFO
      inputBinding:
        position: -2
        prefix: "--loglevel"
    ignore_seed_reactions:
      type: boolean?
      doc: |
        ignore reactions with empty products or reactions
      default: False
      inputBinding:
        position: -1
        prefix: "--ignore-seed-reactions"
  
outputs:
  json:
    type: File?
    outputBinding:
      glob: $(runtime.outdir)/$(inputs.json_output)
  csv:
    type: File?
    outputBinding:
      glob: $(runtime.outdir)/$(inputs.csv_output)

baseCommand:
  - "cocomico"

arguments:
  - "community"


s:author:
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005

s:codeRepository: https://gitlab.inria.fr/CCMC/CoCoMiCo.git
s:license: https://spdx.org/licenses/GPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2023-08-29"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.25.owl
