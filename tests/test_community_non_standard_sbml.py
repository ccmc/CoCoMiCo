"""Community class feature tests."""

from pathlib import Path

import pytest
from pytest_bdd import given, scenario, then, when

from cocomico.base import Taxon
from cocomico.community import Community


@scenario("community_non_standard_sbml.feature", "Handle SBML no id")
def test_handle_sbml_no_id():
    """Handle SBML no id."""


@scenario("community_non_standard_sbml.feature", "Handle SBML no model")
def test_handle_sbml_no_model():
    """Handle SBML no model."""


@scenario("community_non_standard_sbml.feature", "Handle SBML no namespace")
def test_handle_sbml_no_namespace():
    """Handle SBML no namespace."""


@scenario("community_non_standard_sbml.feature", "Handle SBML no sbml")
def test_handle_sbml_no_sbml():
    """Handle SBML no sbml."""


@scenario("community_non_standard_sbml.feature", "Handle SBML parse error")
def test_handle_sbml_parse_error():
    """Handle SBML parse error."""


@scenario("community_non_standard_sbml.feature", "Handle SBML not relative")
def test_handle_sbml_not_relative():
    """Handle SBML not relative."""


@scenario("community_non_standard_sbml.feature", "Handle SBML no sbml_dir")
def test_handle_sbml_no_sbml_dir():
    """Handle SBML no sbml_dir."""


@given("SBML models directory", target_fixture="sbml_dir")
def _() -> Path:
    """SBML models directory."""
    return (Path("tests") / "data_test" / "sbml_nonstd").resolve()


@when("create community object from no_id", target_fixture="no_id")
def _(sbml_dir) -> Community:
    """create community object from no_id."""
    return Community(models=[sbml_dir / "model_bare.sbml"], sbml_dir=sbml_dir)


@when("create community object from no_model", target_fixture="no_model")
def _(sbml_dir) -> Community:
    """create community object from no_model."""
    with pytest.raises(ValueError):
        return Community(models=[sbml_dir / "model_no_model.sbml"], sbml_dir=sbml_dir)
    return Community()  # mask no return value when ValueError raised


@when("create community object from no_ns", target_fixture="no_ns")
def _(sbml_dir) -> Community:
    """create community object from no_ns."""
    return Community(models=[sbml_dir / "model_no_ns.sbml"], sbml_dir=sbml_dir)


@when("create community object from no_sbml", target_fixture="no_sbml")
def _(sbml_dir) -> Community:
    """create community object from no_sbml."""
    with pytest.raises(ValueError):
        return Community(models=[sbml_dir / "model_no_sbml.sbml"], sbml_dir=sbml_dir)
    return Community()  # mask no return value when ValueError raised


@when("create community object from invalid expecting exception")
def _(sbml_dir):
    """create community object from invalid expecting exception."""
    with pytest.raises(ValueError):
        _ = Community(models=[sbml_dir / "invalid.sbml"], sbml_dir=sbml_dir)


@when("create community object not relative to sbml_dir", target_fixture="not_relative")
def _(sbml_dir) -> Community:
    """create community object not relative to sbml_dir."""
    return Community(
        models=[(Path("tests") / "data_test" / "sbml" / "Com1Org1.sbml").resolve()],
        sbml_dir=sbml_dir,
    )


@when("create community object without sbml_dir", target_fixture="not_relative")
def _() -> Community:
    """create community object without sbml_dir."""
    return Community(
        models=[(Path("tests") / "data_test" / "sbml" / "Com1Org1.sbml").resolve()],
    )


@then("no_ns model taxon is as expected")
def _(no_ns):
    """no_ns model taxon is as expected."""
    assert {Taxon("Com1Org1")} == no_ns.taxa


@then("no_id model taxon is the file stem")
def _(no_id):
    """no_id model taxon is the file stem."""
    assert {Taxon("model_bare")} == no_id.taxa


@then("no_model community is empty")
def _(no_model):
    """no_model community is empty."""
    assert not no_model


@then("no_sbml community is empty")
def _(no_sbml):
    """no_sbml community is empty."""
    assert not no_sbml


@then("exception was raised")
def _():
    """exception was raised"""
    # If we get here in the "Handle SBML parse error" scenario without pytest reporting
    # Failed: DID NOT RAISE <class 'xml.etree.ElementTree.ParseError'>
    # then the exception was raised.
    # pass


@then("path is not relativized")
def _(not_relative, sbml_dir):
    """path is not relativized."""
    assert {Taxon("Com1Org1")} == not_relative.taxa

    path = not_relative.files[Taxon("Com1Org1")]
    assert path.stem == "Com1Org1"
    with pytest.raises(ValueError):
        assert not path.relative_to(sbml_dir)
