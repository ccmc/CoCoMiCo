"""Common fixtures and steps."""

from pathlib import Path
from typing import List

import pytest
from click.testing import CliRunner
from pytest_bdd import given, parsers, when

from cocomico import cwl
from cocomico.base import Biomolecule, Seeds
from cocomico.community import Community
from sampling.name import Name

# Common fixtures from test data.
# Resolve to absolute paths in case cwd changes.


@given("seed file", target_fixture="seed_file")
def _() -> Path:
    """seed file."""
    return (Path("tests") / "data_test" / "seeds.sbml").resolve()


@given("a community definition file", target_fixture="community_json")
def _() -> Path:
    """a community definition file."""
    return (Path("tests") / "data_test" / "communities.json").resolve()


@given("SBML models directory", target_fixture="sbml_dir")
def _() -> Path:
    """SBML models directory."""
    return (Path("tests") / "data_test" / "sbml").resolve()


@given("model files", target_fixture="models")
def _(sbml_dir: Path) -> List[Path]:
    """model files."""
    return [
        sbml_dir / f"Com{ i }Org{ j }.sbml"
        for i, j in [(2, 1), (2, 2), (2, 3), (2, 4), (1, 2)]
    ]


# Common fixture from constant data.


@given("seeds", target_fixture="seeds")
def _() -> Seeds:
    """seeds."""
    return Seeds(
        seeds=(Biomolecule(m) for m in ["M_E_c", "M_F_c", "M_X1_c"]), name="seeds"
    )


# Common fixtures for building communities.


@given(parsers.parse("community of {organisms:S}"), target_fixture="organisms")
def _(organisms: str) -> List[str]:
    """community of <organisms>."""
    return organisms.split(",")


@when("build community", target_fixture="community")
def _(organisms: List[str], sbml_dir: Path) -> Community:
    """build community."""
    files = [Path(f"{model}.sbml") for model in organisms]
    community = Community(models=files, sbml_dir=sbml_dir)
    assert not community.seeds()
    return community


# Other common fixtures.


@when("compute results for seeds")
def _(community, seeds):
    """compute results for seeds."""
    assert community.scope(seeds)
    assert community.exchange(seeds)
    assert community.seeds()


@when("run cocomico example")
def _(output):
    """run cocomico example."""
    runner = CliRunner()
    result = runner.invoke(
        cwl.entrypoint,
        [
            "example",
            "--output",
            output,
        ],
    )
    assert result.exit_code == 0


@when("run cocomico example symbolic seeds")
def _(output):
    """run cocomico example symbolic seeds."""
    runner = CliRunner()
    result = runner.invoke(
        cwl.entrypoint,
        [
            "--symbolic-seeds",
            "example",
            "--output",
            output,
        ],
    )
    assert result.exit_code == 0


# Helper fixture with reusable static functions.


class Helpers:
    """Static helper functions for test steps."""

    # pylint: disable=R0903
    @staticmethod
    def invoke(
        # pylint: disable=R0913
        command: str,
        seed: Path,
        sbml: Path,
        output: Path | None = None,
        json: Path | None = None,
        csv: Path | None = None,
        models: List[Path] | None = None,
        extra: List[str] | None = None,
    ):
        """Helper to invoke the CLI."""
        args = [
            command,
            "--seeds-file",
            str(seed),
            "--sbml-dir",
            str(sbml),
        ]
        if command == "community":
            args.extend(["--name", "erehwon"])
        if output:
            args.extend(["--output", str(output)])
        if json:
            args.extend(["--json-output", str(json)])
        if csv:
            args.extend(["--csv-output", str(csv)])
        if models:
            args.extend([str(f) for f in models])
        if extra:
            args.extend([str(i) for i in extra])

        runner = CliRunner()
        return runner.invoke(cwl.entrypoint, args)


@pytest.fixture
def helpers():
    """Fixture to provide static functions to test steps."""
    return Helpers


def pytest_bdd_before_scenario(
    request,  # pylint: disable=unused-argument
    feature,  # pylint: disable=unused-argument
    scenario,
):
    """Pytest hook to reset sampling Name."""
    if "reset-name" in scenario.tags:
        Name.serial.clear()
