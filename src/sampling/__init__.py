# -*- coding: utf-8 -*-

# Copyright (C) 2022 Maxime Lecomte - David Sherman - Clémence Frioux - Inria BSO - Pleiade
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


"""
CoCoMiCo sampling generates community specifications from existing communities.
"""

import logging
import pickle
import random
from pathlib import Path
from typing import Sized


def ess(x: Sized) -> str:
    """Final 's' if plural."""
    return "s"[: (len(x) - 1 if (x and len(x)) else 1)]


# Serialize random number generator state.
def save_state(where: Path, state=None) -> None:
    """Save random number generator state to out directory."""
    state_file = where / "random-state.pickle" if where.is_dir() else where
    with open(state_file, "wb") as f:
        logging.debug("State: writing %s/%s", state_file.parent, state_file.name)
        pickle.dump(state or random.getstate(), f)


def load_state(where: Path) -> None:
    """Save random number generator state to out directory."""
    state = where / "random-state.pickle" if where.is_dir() else where
    with open(state, "rb") as f:
        logging.debug("State: reading %s/%s", state.parent, state.name)
        random.setstate(pickle.load(f))
