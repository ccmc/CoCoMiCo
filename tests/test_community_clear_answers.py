"""Community class feature tests."""

from sys import getsizeof

from pytest import approx
from pytest_bdd import parsers, scenario, then, when

from cocomico import score
from cocomico.constants import COMP_NUM, COOP_CONS, COOP_NUM, COOP_PROD

# pylint: disable=W0212


@scenario("community_clear_answers.feature", "Clear answers")
def test_clear_answers():
    """Clear answers."""


@when("clear answers")
def _(community):
    """clear answers."""
    community.clear_answers()


@when("get initial memory", target_fixture="memory_initial")
def _(community):
    """get initial memory."""
    return getsizeof(community._answers)


@when("get cleared memory", target_fixture="memory_cleared")
def _(community):
    """get cleared memory."""
    return getsizeof(community._answers)


@when("get first memory", target_fixture="memory_first")
def _(community):
    """get first memory."""
    return getsizeof(community._answers)


@when("get first results", target_fixture="results_first")
def _(community, seeds):
    """get first results."""
    return score.cooperation(community, seeds), score.competition(community, seeds)


@when("get second results", target_fixture="results_second")
def _(community, seeds):
    """get second results."""
    return score.cooperation(community, seeds), score.competition(community, seeds)


@then(
    parsers.parse(
        "first results are "
        "{coop:g} {coop_num:g} {coop_prod:g} {coop_cons:g} {comp:g} {comp_num:g}"
    )
)
def _(results_first, coop, coop_num, coop_prod, coop_cons, comp, comp_num):
    """first results are <coop> <coop_num> <coop_prod> <coop_cons> <comp> <comp_num>."""
    # pylint: disable=R0913
    computed_coop, computed_coop_metrics = results_first[0]
    assert coop == approx(computed_coop, 0.01)
    assert coop_num == computed_coop_metrics[COOP_NUM]
    assert coop_prod == approx(computed_coop_metrics[COOP_PROD], 0.01)
    assert coop_cons == approx(computed_coop_metrics[COOP_CONS], 0.01)
    computed_comp, computed_comp_metrics = results_first[1]
    assert comp == approx(computed_comp, 0.01)
    assert comp_num == approx(computed_comp_metrics[COMP_NUM])


@then("second results match first results")
def _(results_first, results_second):
    """second results match first results."""
    coop1, coop_metrics1 = results_first[0]
    coop2, coop_metrics2 = results_second[0]
    assert coop1 == coop2
    assert coop_metrics1[COOP_NUM] == coop_metrics2[COOP_NUM]
    assert coop_metrics1[COOP_PROD] == coop_metrics2[COOP_PROD]
    assert coop_metrics1[COOP_CONS] == coop_metrics2[COOP_CONS]
    comp1, comp_metrics1 = results_first[1]
    comp2, comp_metrics2 = results_second[1]
    assert comp1 == comp2
    assert comp_metrics1[COMP_NUM] == comp_metrics2[COMP_NUM]


@then("first memory is not empty")
def _(memory_first):
    """first memory is not empty."""
    assert memory_first > getsizeof({})


@then("initial and cleared memory are empty")
def _(memory_initial, memory_cleared):
    """initial and cleared memory are empty."""
    assert memory_initial == getsizeof({})
    assert memory_cleared == getsizeof({})
