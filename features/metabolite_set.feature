Feature: MetaboliteSet class
  Represent sets of taxon-tagged metabolites

  Scenario Outline: Get taxa and select subsets from metabolite sets
  Given <metabolite_set> of metabolite strings
  And a set of <select_taxa>
  And a set of metabolite <biomolecules>
  When create metabolite set
  And select a subset with select_taxa
  Then metabolite set has all possible taxa
  And metabolite set contains select_taxa
  And the subset gives a set with biomolecules
  And the taxa of the subset are only select_taxa 
  And the string representation is correct

  Examples:
  | metabolite_set              | select_taxa  | biomolecules   |
  | A.1,B.1,C.1,C.2,D.2,A.3,C.3 | 1,2,3        | A,B,C,D |
  | A.1,B.1,C.1,C.2,D.2,A.3,C.3 | 2,3          | A,C,D   |
