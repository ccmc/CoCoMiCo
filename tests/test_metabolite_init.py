"""MetaboliteSet class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, Taxon


@scenario(
    "metabolite_init.feature",
    "Initialize a metabolite from provenance and biomolecule",
)
def test_initialize_a_metabolite_from_provenance_and_biomolecule():
    """Initialize a metabolite from provenance and biomolecule."""


@given(parsers.parse("a {provenance:S} taxon"), target_fixture="provenance")
def _(provenance: Taxon):
    """a <provenance> taxon."""
    return provenance


@given(parsers.parse("a metabolite {biomolecule:S}"), target_fixture="biomolecule")
def _(biomolecule: Biomolecule):
    """a metabolite <biomolecule>."""
    return biomolecule


@when("create metabolite", target_fixture="metabolite")
def _(provenance: Taxon, biomolecule: Biomolecule):
    """create metabolite."""
    return Metabolite(provenance=provenance, biomolecule=biomolecule)


@then("metabolite has the same biomolecule")
def _(metabolite: Metabolite, biomolecule: Biomolecule):
    """metabolite has the same <biomolecule>."""
    assert metabolite.biomolecule == biomolecule


@then("metabolite has the same provenance")
def _(metabolite: Metabolite, provenance: Taxon):
    """metabolite has the same <provenance>."""
    assert metabolite.provenance == provenance


@then(parsers.parse("metabolite has expected {string:S}"))
def _(metabolite: Metabolite, string: str):
    """metabolite has expected <string>."""
    assert str(metabolite) == string
    assert eval(repr(metabolite)) == metabolite  # pylint: disable=W0123
