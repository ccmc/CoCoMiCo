Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Invalid seed file
    Given invalid seed file
    And SBML models directory
    And model files
    When run cocomico
    Then fail wth error

  Scenario Outline: Invalid SBML directory
    Given seed file
    And invalid SBML models directory
    And model files
    When run cocomico
    Then fail wth error

  Scenario Outline: Invalid SBML directory is a file
    Given seed file
    And invalid SBML models directory is a file
    And model files
    When run cocomico
    Then fail wth error

  Scenario Outline: Invalid model files
    Given seed file
    And SBML models directory
    And invalid model files
    When run cocomico
    Then fail wth error
