"""Community class feature tests."""

from pathlib import Path

from pytest_bdd import given, scenario, then, when

from cocomico.community import Community


@scenario("community_remove_seed_reactions.feature", "Ignore seed reactions")
def test_ignore_seed_reactions():
    """Ignore seed reactions."""


@given("SBML noexchange directory", target_fixture="sbml_dir_noexch")
def _():
    """SBML noexchange directory."""
    return (Path("tests") / "data_test" / "sbml_noexch").resolve()


@when("create community object from noexch directory", target_fixture="community")
def _(sbml_dir_noexch: Path) -> Community:
    """create community object from noexch directory."""
    community = Community(sbml_dir=sbml_dir_noexch)
    files = [str(f.stem) for f in sbml_dir_noexch.iterdir()]
    assert set(community.keys()) == set(files)
    return community


@when("remove seed reactions")
def _(community):
    """remove seed reactions."""
    assert 76 == len(community.reactions)
    community.remove_seed_reactions()
    assert 76 - 4 == len(community.reactions)


@then("all reactions have reactants and products")
def _(community):
    """all reactions have reactants and products."""
    for rea in community.reactions:
        taxon = rea.taxon
        rr = [
            m for m, r, _ in community.models[taxon].relations["reactant"] if r == rea
        ]
        rp = [m for m, r, _ in community.models[taxon].relations["product"] if r == rea]
        assert rp and rr
