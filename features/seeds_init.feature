Feature: Seeds class
  Represent a collection of seeds

  Scenario Outline: Initialize seeds
    Given a set of <metabolite_biomolecules>
    And a metabolite list <metabolite_list>
    When create anonymous seeds object
    Then anon object contains expected metabolites

    Examples:
    | metabolite_biomolecules   | metabolite_list     |
    | M_E_c,M_F_c,M_X1_c        | M_E_c,M_F_c,M_X1_c  |
    | A,B,A,C,A,B               | A,B,C               |

  Scenario Outline: Initialize seeds using kwarg
    Given a set of <metabolite_biomolecules>
    And a metabolite list <metabolite_list>
    When create anonymous seeds object using kwarg
    Then anon object contains expected metabolites

    Examples:
    | metabolite_biomolecules   | metabolite_list     |
    | M_E_c,M_F_c,M_X1_c        | M_E_c,M_F_c,M_X1_c  |
    | A,B,A,C,A,B               | A,B,C               |

  Scenario Outline: Initialize named seeds
    Given a set of <metabolite_biomolecules>
    And a metabolite list <metabolite_list>
    And a name <name>
    When create named seeds object
    Then named object contains expected metabolites
    And named object has expected name

    Examples:
    | metabolite_biomolecules   | metabolite_list     | name        |
    | M_E_c,M_F_c,M_X1_c        | M_E_c,M_F_c,M_X1_c  | minimal     |
    | A,B,A,C,A,B               | A,B,C               | some_seeds  |
