Feature: Community class
  Represent a microbial community

  Scenario Outline: Initialize a community
    Given SBML models directory
    When create community object from <models>
    Then the community has expected <taxa> <products> <reactants> <reactions>

    Examples:
    | models | taxa | products | reactants | reactions |
    | Com1Org1,Com1Org2,Com1Org3 | Com1Org1,Com1Org2,Com1Org3 | M_A2_c,M_A_c,M_B2_c,M_B_c,M_C2_c,M_C_c,M_D_c,M_F_c,M_F2_c,M_G_c,M_H_c,M_I_c,M_J_c,M_M_c,M_O_c,M_P_c,M_Q_c,M_S_c,M_V_c,M_W_c,M_X_c,M_Y_c | M_A2_c,M_A_c,M_B2_c,M_B_c,M_C2_c,M_C_c,M_D_c,M_E_c,M_F2_c,M_F_c,M_G_c,M_H_c,M_I_c,M_K_c,M_M_c,M_N_c,M_O_c,M_P_c,M_Q_c,M_S_c,M_T_c,M_V_c,M_W_c,M_X1_c,M_X_c,M_Y_c,M_Z_c | R_A2_B2,R_AZ_W,R_A_S,R_B_JD,R_C2_B2,R_C_C2,R_C_PY,R_D_X,R_E_V,R_F2_C,R_F_M,R_G_H,R_H_C,R_I_H,R_K_H,R_M_O,R_M_Q,R_N_M,R_O_P,R_Q_A,R_T_S,R_V_B,R_W_A2,R_W_B,R_X1_F2,R_X_G,R_Y_W,R_O_Prev,R_M_Qrev,R_C_C2rev,R_W_A2rev,R_X_Grev,R_A_Srev,R_V_Brev,R_A2_B2rev,R_F_Mrev,R_C2_B2rev,R_D_Xrev,R_Q_Arev,R_F2_Crev,R_I_Hrev,R_G_Hrev |
    | None   | None | None     | None      | None      |
