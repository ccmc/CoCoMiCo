"""Model class feature tests."""

from typing import Sequence

from pytest_bdd import given, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, Reaction, Taxon
from cocomico.model import Model


@scenario("model.feature", "Initialize a model")
def test_initialize_a_model():
    """Initialize a model."""


@scenario("model.feature", "Initialize a model ignoring seed reactions")
def test_initialize_a_model_ignoring_seed_reactions():
    """Initialize a model ignoring seed reactions."""


@given("a taxon", target_fixture="taxon")
def _():
    """a taxon."""
    return Taxon("Org")


@given("a set of metabolites", target_fixture="some_metabolites")
def _(taxon) -> Sequence[Metabolite]:
    """a set of metabolites."""
    biomolecule = [
        Metabolite(biomolecule=Biomolecule(biomolecule), provenance=taxon)
        for biomolecule in ["A", "B", "C", "D"]
    ]
    return biomolecule


@given("a set of reactions", target_fixture="some_reactions")
def _(taxon) -> Sequence[Reaction]:
    """a set of reactions."""
    reactions = [
        Reaction(name=biomolecule, taxon=taxon)
        for biomolecule in ["R1", "R2", "R2rev", "SR3", "SR4", "SR4rev"]
    ]
    return reactions


@given("a set of metabolite-reaction relations", target_fixture="edges")
def _(some_metabolites, some_reactions) -> set[tuple[Metabolite, Reaction]]:
    """a set of metabolite-reaction relations."""
    tuples = {
        # A + B --R1--> C
        (some_metabolites[0], some_reactions[0]),
        (some_metabolites[1], some_reactions[0]),
        (some_reactions[0], some_metabolites[2]),
        # C <--R2--> D
        (some_metabolites[2], some_reactions[1]),
        (some_reactions[1], some_metabolites[3]),
        (some_metabolites[3], some_reactions[2]),
        (some_reactions[2], some_metabolites[2]),
        # ∅ --SR3--> C  # (seed reaction)
        (some_reactions[3], some_metabolites[2]),
        # A --SR4--> ∅  # (seed reaction)
        (some_metabolites[0], some_reactions[4]),
        # B <--SR4--> ∅  # (reversible seed reaction)
        (some_metabolites[1], some_reactions[4]),
        (some_reactions[5], some_metabolites[1]),
    }
    return tuples


@when("create model object", target_fixture="model")
def _(some_metabolites, edges):
    """create model object."""
    model = Model(biomolecule={*some_metabolites}, tuples={*edges})
    assert len(edges) == len(model.relations["product"]) + len(
        model.relations["reactant"]
    )
    return model


@when("create model object ignoring seed reactions", target_fixture="model")
def _(some_metabolites, edges):
    """create model objectignoring seed reactions."""
    model = Model(biomolecule={*some_metabolites}, tuples={*edges})
    assert len(edges) == len(model.relations["product"]) + len(
        model.relations["reactant"]
    )

    model.remove_seed_reactions()
    rea = [r for _, r, _ in [*model.relations["reactant"], *model.relations["product"]]]
    not_seed = sum(
        1
        for r in rea
        if any(True for mrt in model.relations["reactant"] if mrt[1] == r)
        or any(True for mrt in model.relations["product"] if mrt[1] == r)
    )
    assert not_seed == len(model.relations["product"]) + len(
        model.relations["reactant"]
    )
    return model


@then("model reactions are as expected")
def _(model, some_reactions):
    """model reactions are as expected."""
    assert model.reactions == {*some_reactions}


@then("model reactions are as expected without seed reactions")
def _(model, some_reactions):
    """model reactions are as expected."""
    seed_reac = {r for r in some_reactions if r.name.startswith("SR")}
    assert model.reactions == {*some_reactions} - seed_reac


@then("model relations are as expected")
def _(model, edges):
    """model relations are as expected."""
    r_rel = [(m, r) for m, r in edges if isinstance(m, Metabolite)]
    p_rel = [(r, m) for r, m in edges if isinstance(m, Metabolite)]

    for m, r in r_rel:
        assert (m, r, m.provenance) in model.relations["reactant"]

    for r, m in p_rel:
        assert (m, r, m.provenance) in model.relations["product"]


@then("model relations are as expected without seed reactions")
def _(model, edges, some_reactions):
    """model relations are as expected."""
    seed_reac = {r for r in some_reactions if r.name.startswith("SR")}
    r_rel = [
        (m, r) for m, r in edges if isinstance(m, Metabolite) and r not in seed_reac
    ]
    p_rel = [
        (r, m) for r, m in edges if isinstance(m, Metabolite) and r not in seed_reac
    ]

    for m, r in r_rel:
        assert (m, r, m.provenance) in model.relations["reactant"]

    for r, m in p_rel:
        assert (m, r, m.provenance) in model.relations["product"]


@then("model biomolecule are as expected")
def _(model, some_metabolites):
    """model biomolecule are as expected."""
    assert model.biomolecule == {*some_metabolites}
