Feature: Community
    Characterize potential for competition and cooperation in a community

    Scenario Outline: Compute metrics
        Given community of <organisms>
        And SBML models directory
        And seeds
        When build community
        And we compute metrics
        Then each <organisms> has the expected <coop> <coop_num> <coop_prod> <coop_cons> <comp> <comp_num>

        Examples:
        | organisms                                    | coop | coop_num | coop_prod | coop_cons | comp | comp_num |
        | Com1Org1,Com1Org2,Com1Org3                   | 9.0  | 4        | 4.5       | 4.5       | 0.66 | 1        |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4          | 17.0 | 8        | 8.5       | 8.5       | 1.0  | 2        |
        | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | 19.25| 8        | 10.0      | 9.25      | 1.8  | 4        |
        | Com1Org1,Com2Org4                            | 2.0  | 1        | 1.0       | 1.0       | 1.0  | 1        |
        | Com3Org1,Com3Org2,Com3Org3,Com3Org4          | 17.0 | 8        | 8.5       | 8.5       | 1.0  | 2        |
        | Com3Org1,Com3Org2,Com3Org3                   | 9.0  | 4        | 4.5       | 4.5       | 0.66 | 1        |
        | Com3Org1,Com3Org2,Com3Org4                   | 8.0  | 4        | 4.0       | 4.0       | 0.66 | 1        |
