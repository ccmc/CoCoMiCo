"""
Type declarations for sampling.
"""

from pathlib import Path
from typing import Iterable

from .name import Name

Model = str | Path
Community = list[Model]
NamedCommunity = tuple[Name, Community]
Ecosystem = Iterable[NamedCommunity] | Iterable[Community]
