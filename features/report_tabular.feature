Feature: Report
  Extract reports from community results

  Scenario Outline: Tabular output
    Given a community definition file
    And SBML models directory
    And seeds
    And CSV output path
    When create community objects
    And compute results for communities
    And write tabular report
    And reread tabular report
    Then communities and tabular data agree
    And tabular data contains expected columns
    And seed column has expected seeds

  Scenario Outline: Tabular output symbolic seeds
    Given a community definition file
    And SBML models directory
    And seeds
    And CSV output path
    When create community objects
    And compute results for communities
    And write tabular report symbolic seeds
    And reread tabular report
    Then communities and tabular data agree
    And tabular data contains expected columns
    And seed column has symbolic name
