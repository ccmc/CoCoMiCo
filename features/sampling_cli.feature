Feature: Sampling of communities
  Generate communities by sampling communities and ecosystems

  Scenario Outline: Command line with name
    Given SBML models directory
    And output directory
    And source is sbml directory
    When generate named samples <name> <sizes> <reps>
    And read the sample from name.json
    Then there are <n_com> communities
    And there are <n_mod> total models
    And the id pattern matches <id_pattern>

    Examples:
    | name  | sizes | reps | n_com | n_mod | id_pattern                       |
    | pairs | 2     | 10   | 10    | 20    | pairs_([0-9]+)_original_([0-9]+)_([0-9]+) |
    | eco   | 5     | 10   | 10    | 50    | eco_([0-9]+)_original_([0-9]+)_([0-9]+)   |
    | eco   | 2,5   | 10   | 20    | 70    | eco_([0-9]+)_original_([0-9]+)_([0-9]+)   |
    
  Scenario Outline: Command line without name
    Given SBML models directory
    And output directory
    And source is sbml directory
    When generate unnamed samples <sizes> <reps>
    And read the sample from default name
    Then there are <n_com> communities
    And there are <n_mod> total models
    And the id pattern matches <id_pattern>

    Examples:
    | sizes | reps | n_com | n_mod | id_pattern                                  |
    | 2     | 10   | 10    | 20    | X_[A-Z0-9]{32}_original_[0-9]+_[0-9]+ |
    | 5     | 10   | 10    | 50    | X_[A-Z0-9]{32}_original_[0-9]+_[0-9]+ |
    | 2,5   | 10   | 20    | 70    | X_[A-Z0-9]{32}_original_[0-9]+_[0-9]+ |

  Scenario Outline: Command line from JSON spec
    Given a community definition file
    And output directory
    And source is community definition file
    When generate unnamed samples <sizes> <reps>
    And read the sample from default name
    Then there are <n_com> communities
    And there are <n_mod> total models
    And the id pattern matches <id_pattern>

    Examples:
    | sizes | reps | n_com | n_mod | id_pattern                                |
    | 2     | 30   | 30    | 60    | X_[A-Z0-9]{32}_original_[0-9]+_[0-9]+ |

  Scenario Outline: Generate added value samples
    Given SBML models directory
    And output directory
    And source is sbml directory
    When generate added value samples <name> <sizes> <reps>
    And read the sample from name.json
    Then there are <n_com> communities
    And there are <n_minus> minus communities
    And there are <n_added> added communities
    And there are <n_mod> total models
    And the id pattern matches <id_pattern>

    Examples:
    | name  | sizes | reps | n_com | n_minus | n_added | n_mod | id_pattern                     |
    | triad | 3     | 10   | 140   | 30      | 100     | 490   | triad_[0-9]+_[^_]+_[0-9]+_[0-9]+ |
    | eco   | 5     | 10   | 160   | 50      | 100     | 850   | eco_[0-9]+_[^_]+_[0-9]+_[0-9]+   |
    | mimsy | 2,5   | 10   | 290   | 70      | 200     | 1190  | mimsy_[0-9]+_[^_]+_[0-9]+_[0-9]+ |
    | eco   | 3,5   | 10   | 300   | 80      | 200     | 1340  | eco_[0-9]+_[^_]+_[0-9]+_[0-9]+   |

  Scenario Outline: Reproducibility from random seed
    Given a community definition file
    And output directory
    And source is community definition file
    And random seed
    When generate single sample with random seed
    And read the sample from default name
    Then the sample has expected members

  Scenario Outline: Reproducibility from random state
    Given a community definition file
    And output directory
    And source is community definition file
    And random state
    When generate single sample with random state
    And read the sample from default name
    Then the sample has expected members

  Scenario Outline: Split output
    Given a community definition file
    And output directory
    And source is community definition file
    When generate 100 pairs with max 50 per sample
    And read the sample from default name
    Then there are 100 samples in 4 files

  Scenario Outline: No sources
    Given output directory
    When invoke with no sources
    Then nothing is done

  Scenario Outline: Model file source forces anonymous
    Given a community definition file
    And SBML models directory
    And model files
    And output directory
    When generate from source having model files
    Then log shows source was anonymous

  Scenario Outline: Generate using pool
    Given a community definition file
    And output directory
    And a pool
    And source is community definition file
    When generate unnamed samples using pool
    And read the sample from default name
    Then the extras are from the pool

  Scenario Outline: Sources as args or options yield the same
    Given community definition files
    And output directory
    When generate trivial samples using source args
    And generate trivial samples using source options
    Then the samples from both source kinds are the same

  Scenario Outline: Chain sampling
    Given community definition files
    And output directory
    When chain generate samples
    Then all chained samples are found
