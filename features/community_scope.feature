Feature: Community class
  Represent a microbial community

  Scenario Outline: Calculate scope
  Given community of <organisms>
  And SBML models directory
  And <seeds>
  When build community
  And compute scope wrt seeds
  Then scope wrt seeds is <expected_scope>
  And taxa of scope are from the community
  And seeds are now in community answers

  Examples:
  | organisms | seeds | expected_scope |
  | Com1Org1,Com1Org2,Com1Org3 | M_E_c,M_F_c,M_X1_c | M_C_c.Com1Org1,M_D_c.Com1Org1,M_F2_c.Com1Org1,M_G_c.Com1Org1,M_H_c.Com1Org1,M_I_c.Com1Org1,M_J_c.Com1Org1,M_X_c.Com1Org1,M_A_c.Com1Org2,M_F_c.Com1Org2,M_M_c.Com1Org2,M_O_c.Com1Org2,M_P_c.Com1Org2,M_Q_c.Com1Org2,M_S_c.Com1Org2,M_Y_c.Com1Org2,M_A2_c.Com1Org3,M_B2_c.Com1Org3,M_B_c.Com1Org3,M_C2_c.Com1Org3,M_C_c.Com1Org3,M_V_c.Com1Org3,M_W_c.Com1Org3 |
  | Com1Org1,Com1Org2,Com1Org3,Com2Org4 | M_E_c,M_F_c,M_X1_c | M_C_c.Com1Org1,M_D_c.Com1Org1,M_F2_c.Com1Org1,M_G_c.Com1Org1,M_H_c.Com1Org1,M_I_c.Com1Org1,M_J_c.Com1Org1,M_X_c.Com1Org1,M_A_c.Com1Org2,M_F_c.Com1Org2,M_M_c.Com1Org2,M_O_c.Com1Org2,M_P_c.Com1Org2,M_Q_c.Com1Org2,M_S_c.Com1Org2,M_Y_c.Com1Org2,M_A2_c.Com1Org3,M_B2_c.Com1Org3,M_B_c.Com1Org3,M_C2_c.Com1Org3,M_C_c.Com1Org3,M_V_c.Com1Org3,M_W_c.Com1Org3,M_A3_c.Com2Org4,M_B3_c.Com2Org4,M_D3_c.Com2Org4,M_E3_c.Com2Org4,M_F3_c.Com2Org4,M_G3_c.Com2Org4,M_K_c.Com2Org4,M_N_c.Com2Org4,M_P_c.Com2Org4,M_Z_c.Com2Org4 |
