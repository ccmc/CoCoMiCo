"""Sampling of communities feature tests."""

from collections import OrderedDict
from pathlib import Path
from typing import Any, Iterable

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.community import Community
from sampling.name import Name

EcoTuple = tuple[str, int, list[str | Path] | None]


@scenario("sampling_name.feature", "Name serial is initially empty")
def test_name_serial_is_initially_empty():
    """Name serial is initially empty."""


@scenario("sampling_name.feature", "Sample name from ecosystem")
def test_sample_name_from_ecosystem():
    """Sample name from ecosystem."""


@scenario("sampling_name.feature", "Generate names from files")
def test_generate_names_from_files():
    """Generate names from files."""


@scenario("sampling_name.feature", "Sample names from names")
def test_sample_names_from_names():
    """Sample names from names."""


@scenario("sampling_name.feature", "Sample names can be used for dicts")
def test_sample_names_can_be_used_for_dicts():
    """Sample names can be used for dicts."""


@given(
    parsers.parse("an ecosystem {eco:S} {models:S} of {size:S}"),
    target_fixture="eco_tuple",
)
def _(eco, models, size) -> tuple[str, int, list[str]]:
    """an ecosystem <eco> <models> of <size>."""
    eco = eco if eco != "None" else None
    models = models.split(",") if models != "None" else []
    return eco, int(size), [f"{f}.sbml" for f in models]


@given(
    parsers.parse("a name made from an {existing:S} string"),
    target_fixture="existing_name",
)
def _(existing: str) -> Name:
    """a name made from an <existing> string."""
    eco, ident, sub, size, rep = existing.split("_")
    name = Name.from_string(existing)
    assert name.eco == eco
    assert name.ident == ident
    assert name.sub == sub
    assert name.size == int(size)
    assert name.rep == int(rep)
    return name


@when("make 1 sample name with eco and model list", target_fixture="name")
def _(eco_tuple: EcoTuple):
    """make 1 sample name with eco and model list."""
    eco, _, com = eco_tuple
    if eco:
        return Name(eco=eco, com=com)
    return Name(com=com)


@when("make 5 sample names with eco, model list, and reps", target_fixture="name_reps")
def _(eco_tuple: EcoTuple) -> Iterable[Name]:
    """make 5 sample names with eco, model list, and reps."""
    eco, _, com = eco_tuple
    if eco:
        return [Name(eco=eco, com=com, rep=i) for i in range(1, 4)]
    return [Name(com=com, rep=i) for i in range(1, 4)]


@when("make 2 sample names with eco, model list, and subs", target_fixture="name_subs")
def _(eco_tuple: EcoTuple) -> Iterable[Name]:
    """make 2 sample names with eco, model list, and subs."""
    eco, _, com = eco_tuple
    if eco:
        return [Name(eco=eco, com=com, sub=i) for i in ["a", "b"]]
    return [Name(com=com, sub=i) for i in ["a", "b"]]


@when(
    parsers.parse("create community object from {models:S}"), target_fixture="community"
)
def _(sbml_dir: Path, models: str) -> Community:
    """create community object from <models>."""
    if models == "None":
        return Community()

    ids = models.split(",")
    files = [Path(f"{id}.sbml") for id in ids]
    community = Community(models=files, sbml_dir=sbml_dir)
    assert set(community.keys()) == set(ids)
    return community


@when("generate id", target_fixture="generated_id")
def _(community: Community) -> str:
    """generate id."""
    return Name.mk_id(sorted(community.files.values()))


@when("generate name", target_fixture="generated_name")
def _(community: Community) -> Name:
    """generate name."""
    return Name(com=sorted(community.files.values()))


@when("make dicts using names", target_fixture="name_dicts")
def _(
    name_reps: Iterable[Name], name_subs: Iterable[Name]
) -> Iterable[dict[Name, Any]]:
    """make dicts using names."""
    names = [*name_reps, *name_subs]
    tuples = [(n, Any) for n in names]
    return [dict(tuples), OrderedDict(tuples)]


@when(
    parsers.parse(
        "make new name from existing and {eco:S} {sub:S} {rep:S} {size:S} {models:S}"
    ),
    target_fixture="new_name_tuple",
)
def _(
    # pylint: disable=R0913
    eco: str,
    sub: str,
    rep: str,
    size: str,
    models: str,
    existing_name: Name,
) -> tuple[Name, str | None, str | None, int | None, int | None, list[str] | None]:
    """make new name from existing and <eco> <sub> <rep> <size> <models>."""
    com_v = models.split(",") if models and models != "None" else None
    eco_v = eco if eco != "None" else None
    sub_v = sub if sub != "None" else None
    rep_v = int(rep) if rep != "None" else None
    size_v = int(size) if size != "None" else None

    name = Name(
        name=existing_name,
        eco=eco_v,
        sub=sub_v,
        rep=rep_v,
        size=size_v,
        com=com_v,  # type: ignore[arg-type]
    )
    return name, eco_v, sub_v, rep_v, size_v, com_v


@then("name serial is initially empty")
def _():
    """name serial is initially empty."""
    assert len(Name.serial) == 0


@then("name has expected size, sub, rep")
def _(name: Name, eco_tuple: EcoTuple):
    """name has expected size, sub, rep."""
    _, size, _ = eco_tuple
    assert name.sub == "original"
    assert name.size == size
    assert not name.rep


@then("name eco is same as eco if given")
def _(name: Name, eco_tuple: EcoTuple):
    """name eco is same as eco if given."""
    eco, _, _ = eco_tuple
    if eco:
        assert name.eco == eco


@then(parsers.parse("name ident is {ident:S} otherwise"))
def _(ident: str, name: Name, eco_tuple: EcoTuple):
    """name ident is <ident> otherwise."""
    eco, _, _ = eco_tuple
    if not eco:
        assert name.ident == ident


@then(parsers.parse("name has expected {string:S}"))
def _(string: str, name: Name):
    """name has expected string."""
    assert str(name) == string


@then("names have expected size and all reps")
def _(name_reps: Iterable[Name], eco_tuple: EcoTuple):
    """names have expected size and all reps."""
    eco, size, _ = eco_tuple
    for name in name_reps:
        if eco:
            assert name.eco == eco
        assert name.sub == "original"
        assert name.size == size
        assert name.rep in range(1, 4)


@then("names have expected size and all subs")
def _(name_subs: Iterable[Name], eco_tuple: EcoTuple):
    """names have expected size and all subs."""
    eco, size, _ = eco_tuple
    for name in name_subs:
        if eco:
            assert name.eco == eco
        assert name.sub in ["a", "b"]
        assert name.size == size
        assert not name.rep


@then(parsers.parse("generated ident is {ident:S}"))
def _(generated_id: str, ident: str):
    """generated ident is <id>."""
    assert ident == generated_id


@then("generated name is (ident, original, None)")
def _(generated_id: str, generated_name: Name):
    """generated name is (ident, original, None)."""
    assert generated_name.ident == generated_id
    assert generated_name.sub == "original"
    assert generated_name.rep == 0


@then(parsers.parse("string name is ident_original_{size:S}_0"))
def _(size: str, generated_id: str, generated_name: Name):
    """string name is ident_original_0_0."""
    generated_string = str(generated_name)
    assert generated_string == f"X_{generated_id}_original_{size}_0"


@then("generated name with rep is (ident, original, rep)")
def _(community: Community, generated_id: str):
    """generated name with rep is (ident, original, rep)."""
    rep = len(community)
    generated_name_rep = Name(eco=generated_id, rep=rep)
    assert generated_name_rep.rep == rep


@then("dicts have 5 keys")
def _(name_dicts: Iterable[dict[Name, Any]]):
    """dicts have 5 keys."""
    for d in name_dicts:
        assert len(d) == 5


@then(parsers.parse("new name has string {expected:S}"))
def _(expected: str, new_name_tuple):
    """new name has string <expected>."""
    new_name, *_ = new_name_tuple
    assert str(new_name) == expected


@then("new name has requested attributes")
def _(existing_name: Name, new_name_tuple):
    """new name has requested attributes."""
    new_name, eco, sub, rep, size, models = new_name_tuple
    assert new_name.eco == eco or existing_name.eco
    assert new_name.sub == sub or existing_name.sub
    assert new_name.rep == rep or existing_name.rep
    assert new_name.size == size or len(models) if models else existing_name.size
