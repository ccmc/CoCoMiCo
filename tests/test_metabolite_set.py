"""MetaboliteSet class feature tests."""

from pytest_bdd import given, parsers, scenario, then, when

from cocomico.base import Biomolecule, Metabolite, MetaboliteSet, Taxon


@scenario("metabolite_set.feature", "Get taxa and select subsets from metabolite sets")
def test_get_taxa_and_select_subsets_from_metabolite_sets():
    """Get taxa and select subsets from metabolite sets."""


@given(
    parsers.parse("{metabolite_set:S} of metabolite strings"),
    target_fixture="metabolite_set",
)
def _(metabolite_set: str) -> list[str]:
    """<metabolite_set> of metabolite strings."""
    return metabolite_set.split(",")


@given(parsers.parse("a set of {select_taxa:S}"), target_fixture="select_taxa")
def _(select_taxa: str) -> set[Taxon]:
    """a set of <select_taxa>."""
    return {Taxon(i) for i in select_taxa.split(",")}


@given(
    parsers.parse("a set of metabolite {biomolecules:S}"), target_fixture="biomolecules"
)
def _(biomolecules: str) -> set[Biomolecule]:
    """a set of metabolite <biomolecules>."""
    return {Biomolecule(i) for i in biomolecules.split(",")}


@when("create metabolite set", target_fixture="allset")
def _(metabolite_set: set[str]) -> MetaboliteSet:
    """create metabolite set."""
    metabolites = {Metabolite.from_string(s) for s in metabolite_set}
    return MetaboliteSet(metabolites)


@when("select a subset with select_taxa", target_fixture="subset")
def _(allset: MetaboliteSet, select_taxa: set[Taxon]) -> MetaboliteSet:
    """select a subset with select_taxa."""
    return allset.select(select_taxa)


@then("metabolite set contains select_taxa")
def _(allset: MetaboliteSet, select_taxa: set[Taxon]):
    """metabolite set contains select_taxa."""
    assert select_taxa <= allset.taxa


@then("metabolite set has all possible taxa")
def _(allset: MetaboliteSet, metabolite_set: set[str]):
    """metabolite set has all possible taxa."""
    possible = {m.split(".", 1)[1] for m in metabolite_set}
    assert allset.taxa == possible


@then("the subset gives a set with biomolecules")
def _(subset: MetaboliteSet, biomolecules: set[Biomolecule]):
    """the subset gives a set with biomolecules."""
    assert subset.biomolecules == biomolecules


@then("the taxa of the subset are only select_taxa")
def _(subset: MetaboliteSet, select_taxa: set[Taxon]):
    """the taxa of the subset are only select_taxa."""
    assert subset.taxa == select_taxa


@then("the string representation is correct")
def _(allset: MetaboliteSet):
    """the string representation is correct."""
    string = str(allset)
    recovered = MetaboliteSet(
        Metabolite.from_string(s) for s in string.strip("{}").split(",")
    )
    assert recovered == allset
