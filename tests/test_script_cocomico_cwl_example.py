"""CWL entrypoint feature tests."""

import json
from pathlib import Path

from pytest_bdd import given, scenario, then

from cocomico import report


@scenario("script_cocomico_cwl_example.feature", "Analyze communities from an example")
def test_analyze_communities_from_an_example():
    """Analyze communities from an example."""


@scenario(
    "script_cocomico_cwl_example.feature",
    "Analyze communities from an example symbolic seeds",
)
def test_analyze_communities_from_an_example_symbolic_seeds():
    """Analyze communities from an example symbolic seeds."""


@given("an output directory path", target_fixture="output")
def _(tmp_path) -> Path:
    """an output directory path."""
    (output := tmp_path / "results").mkdir(parents=True, exist_ok=True)
    return output


@then("have result files")
def _(output):
    """have result files."""
    assert output.is_dir()
    assert (output / "example.csv").is_file()


@then("JSON file has results")
def _(output):
    """JSON files has results."""
    for community in ["com_0", "com_1", "com_2"]:
        output_json = output / f"{ community }.json"
        with open(output_json, mode="r", encoding="UTF-8") as json_file:
            from_json = json.load(json_file)
            assert "taxa" in from_json
            assert "results" in from_json


@then("tabular file has results")
def _(output):
    """tabular file has results."""
    output_csv = output / "example.csv"
    table = report.read_tabular(output_csv)
    for community in ["com_0", "com_1", "com_2"]:
        assert table[community]


@then("seed column has symbolic name")
def _(output):
    """seed column has symbolic name."""
    output_csv = output / "example.csv"
    table = report.read_tabular(output_csv)
    for name, row in table.items():
        col_seeds = "".join(str(i) for i in row[0])
        assert col_seeds == "seeds"
