Feature: Report
  Extract reports from community analyses

  Scenario Outline: JSON report
  Given community of <organisms>
  And SBML models directory
  And seeds
  And JSON output path
  When build community
  And compute results for seeds	
  And write JSON report
  And reread JSON report
  Then community and JSON report agree
  And JSON report contains expected keys

  Examples:
  | organisms                                    |
  | Com1Org1,Com1Org2,Com1Org3                   |
  | Com1Org1,Com2Org4                            |
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4          |
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 |
  | Com3Org1,Com3Org2,Com3Org3                   |
  | Com3Org1,Com3Org2,Com3Org3,Com3Org4          |
  | Com3Org1,Com3Org2,Com3Org4                   |
