Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Analyze communities from an example
    Given an output directory path
    When run cocomico example
    Then have result files
    And JSON file has results
    And tabular file has results

  Scenario Outline: Analyze communities from an example symbolic seeds
    Given an output directory path
    When run cocomico example symbolic seeds
    Then have result files
    And JSON file has results
    And tabular file has results
    And seed column has symbolic name
