"""Report feature tests."""

import csv
from pathlib import Path

from pytest import approx
from pytest_bdd import given, scenario, then, when

from cocomico import report, score
from cocomico.base import Biomolecule, Seeds
from cocomico.community import Community
from cocomico.constants import COMP_NUM, COOP_CONS, COOP_NUM, COOP_PROD


@scenario("report_tabular.feature", "Tabular output")
def test_tabular_output():
    """Tabular output."""


@scenario("report_tabular.feature", "Tabular output symbolic seeds")
def test_tabular_output_symbolic_seeds():
    """Tabular output symbolic seeds."""


@given("CSV output path", target_fixture="output_path")
def _(tmp_path):
    """CSV output path."""
    return tmp_path / "community.tsv"


@when("create community objects", target_fixture="communities")
def _(community_json: Path, sbml_dir: Path) -> dict[str, Community]:
    """create community objects."""
    return Community.from_specification(spec=community_json, sbml_dir=sbml_dir)


@when("compute results for communities")
def _(communities, seeds):
    """compute results for communities."""
    for community in communities.values():
        _ = community.scope(seeds)
        assert community.exchange(seeds)


@when("write tabular report")
def _(communities, output_path):
    """write tabular report."""
    report.write_tabular(communities, output_path)


@when("write tabular report symbolic seeds")
def _(communities, output_path):
    """write tabular report symbolic seeds."""
    report.write_tabular(
        communities=communities, output_file=output_path, symbolic_seeds=True
    )


@when("reread tabular report", target_fixture="table")
def _(output_path):
    """reread tabular report."""
    return report.read_tabular(output_path)


@then("communities and tabular data agree")
def _(communities, table, seeds):
    """communities and tabular data agree."""
    for name, row in table.items():
        community = communities[name]

        coop, coop_metrics = score.cooperation(community, seeds)
        comp, comp_metrics = score.competition(community, seeds)
        delta, _ = score.delta(community, seeds)
        rho, _ = score.rho(community, seeds)

        assert delta == approx(float(row[1]), 0.1)
        assert coop == approx(float(row[2]), 0.1)
        assert rho == approx(float(row[3]), 0.1)
        assert comp == approx(float(row[4]), 0.1)
        assert comp_metrics[COMP_NUM] == int(row[5])
        assert coop_metrics[COOP_NUM] == int(row[6])
        assert coop_metrics[COOP_PROD] == approx(float(row[7]), 0.1)
        assert coop_metrics[COOP_CONS] == approx(float(row[8]), 0.1)
        assert len(community) == int(row[9])


@then("tabular data contains expected columns")
def _(output_path):
    """tabular data contains expected columns."""
    expected = set(["community", "seeds", "coop", "comp", "size"])

    # Read header line from CSV file and assert expected columns
    with open(output_path, mode="r", encoding="UTF-8") as input_csv:
        reader = csv.reader(input_csv, dialect="excel-tab")
        colnames = set(next(reader))
        assert expected <= colnames


@then("seed column has expected seeds")
def _(table, seeds):
    """seed column has expected seeds."""
    for name, row in table.items():
        col_seeds = Seeds(Biomolecule(i) for i in row[0])
        assert col_seeds == seeds


@then("seed column has symbolic name")
def _(table):
    """seed column has symbolic name."""
    for name, row in table.items():
        col_seeds = "".join(str(i) for i in row[0])
        assert col_seeds == "seeds"
