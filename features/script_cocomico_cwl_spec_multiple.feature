Feature: CWL entrypoint
  Wrap CoCoMiCo for Common Workflow Language

  Scenario Outline: Analyze multiple specs from options
    Given seed file
    And SBML models directory
    And output directory path
    And directory with spec files
    And multiple specs <spec_files>
    When run cocomico spec options
    Then have expected <communities>

    Examples:
    | spec_files                          | communities             |
    | spec_0.json                         | com_0,com_1             |
    | spec_0.json,spec_1.json             | com_0,com_1,com_2       |
    | spec_0.json,spec_1.json,spec_2.json | com_0,com_1,com_2,com_3 |

  Scenario Outline: Analyze multiple specs from args
    Given seed file
    And SBML models directory
    And output directory path
    And directory with spec files
    And multiple specs <spec_files>
    When run cocomico spec args
    Then have expected <communities>

    Examples:
    | spec_files                          | communities             |
    | spec_0.json                         | com_0,com_1             |
    | spec_0.json,spec_1.json             | com_0,com_1,com_2       |
    | spec_0.json,spec_1.json,spec_2.json | com_0,com_1,com_2,com_3 |

  Scenario Outline: Analyze multiple specs from directory
    Given seed file
    And SBML models directory
    And output directory path
    And directory with spec files
    When run cocomico spec directory
    Then have expected <communities>

    Examples:
    | communities             |
    | com_0,com_1,com_2,com_3 |
