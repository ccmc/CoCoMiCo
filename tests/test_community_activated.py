"""Community class feature tests."""

from typing import Union

from pytest_bdd import parsers, scenario, then, when

from cocomico.base import Reaction, Seeds, Taxon
from cocomico.community import Community


@scenario("community_activated.feature", "Calculate activated")
def test_calculate_activated():
    """Calculate activated."""


@when(parsers.parse("choose {community_choice:S}"), target_fixture="choice")
def _(community_choice) -> Union[Taxon, None]:
    """choose <community_choice>."""
    # Sentinal value because pytest-bdd has troubel parsing empty example value
    if community_choice is None or community_choice in ["all", "None"]:
        return None
    return Taxon(community_choice)


@when("compute activated wrt seeds for community choice", target_fixture="activated")
def _(choice: Union[Taxon, None], community: Community, seeds: Seeds) -> set[Reaction]:
    """compute activated wrt seeds for community choice."""
    if choice is not None:
        # Explicitly provide optional choice parameter
        return community.activated(seeds, choice=choice)
    # Explicitly do not provide optional choice parameter
    return community.activated(seeds)


@then(parsers.parse("activated wrt seeds for choice is {expected_activated:S}"))
def _(choice: Union[Taxon, None], expected_activated: str, activated: set[Reaction]):
    """activated wrt seeds for choice is <expected_activated>."""
    # Compare observed and expected reaction names
    expected = set(expected_activated.split(","))
    observed = {r.name for r in activated}
    assert observed == expected

    # Check that taxa of activated are the same taxon, if choice is not None
    if choice is not None:
        for m in activated:
            assert m.taxon == choice
