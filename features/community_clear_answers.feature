Feature: Community class
  Represent a microbial community

  Scenario Outline: Clear answers
  Given community of <organisms>
  And SBML models directory
  And seeds
  When build community
  And get initial memory
  And get first results
  And get first memory
  And clear answers
  And get cleared memory
  And get second results
  Then first results are <coop> <coop_num> <coop_prod> <coop_cons> <comp> <comp_num>
  And second results match first results
  And initial and cleared memory are empty
  And first memory is not empty

  Examples:
  | organisms                                    | coop | coop_num | coop_prod | coop_cons | comp | comp_num |
  | Com1Org1,Com1Org2,Com1Org3                   | 9.0  | 4        | 4.5       | 4.5       | 0.66 | 1        |
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4          | 17.0 | 8        | 8.5       | 8.5       | 1.0  | 2        |
  | Com2Org1,Com2Org2,Com2Org3,Com2Org4,Com1Org2 | 19.25| 8        | 10.0      | 9.25      | 1.8  | 4        |
